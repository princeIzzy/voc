const Commercial = ({title,duration}) => {
    return (
        <div className='cu-pink w-100'>
            <div className='p-3 cu-orange d-md-flex justify-content-between'>
                <div className=' clash fs-5'>{title}</div>
                <div className='raleway fs-6 fw-bold'>{duration}</div>
            </div>
        </div>
    );
};
export default Commercial;