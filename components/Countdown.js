import React, { useState, useEffect } from 'react';

const Countdown = ({ targetDate }) => {
    const [timeLeft, setTimeLeft] = useState({ days: 0, hours: 0, minutes: 0, seconds: 0 });
  
    useEffect(() => {
      const interval = setInterval(() => {
        const now = new Date().getTime();
        const difference = targetDate - now;
  
        if (difference > 0) {
          const days = Math.floor(difference / (1000 * 60 * 60 * 24));
          const hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
          const seconds = Math.floor((difference % (1000 * 60)) / 1000);
  
          setTimeLeft({ days, hours, minutes, seconds });
        } else {
          clearInterval(interval);
          setTimeLeft({ days: 0, hours: 0, minutes: 0, seconds: 0 });
        }
      }, 1000);
  
      return () => clearInterval(interval);
    }, [targetDate]);
  
    return (
            <ul className='d-flex col-12 col-lg-5 text-white my-auto list-unstyled'>
              <li className='mx-auto'>
                <span className='clash fs-5 pe-2'>{timeLeft.days}</span>
                <span>days</span>
              </li>
              <li className='mx-auto'>
                <span className='clash fs-5 pe-2'>{timeLeft.hours}</span>
                <span>hours</span>
              </li>
              <li className='mx-auto'>
                <span className='clash fs-5 pe-2'>{timeLeft.minutes}</span>
                <span>minutes</span>
              </li>
              <li className='mx-auto'>
                <span className='clash fs-5 pe-2'>{timeLeft.seconds}</span>
                <span>seconds</span>
              </li>
            </ul>     
    );
  };
  export default Countdown;