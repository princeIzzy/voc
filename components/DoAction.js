/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import Image from 'next/image'
import {faces} from '../../_assets/images'

const DoAction = () => {
  return (
    <div className='register_cont position-relative overflow-hidden'>
      <Image src={faces} alt='banner'  className='position-relative register_img' />
      <div className='position-absolute d-flex flex-column align-items-center justify-content-center w-100 h-100 text-center register_overlay text-white'>
        <h2 className='register_title fs-1'>Register to Attend</h2>
        <p className='opacity-75 mb-5'>Attend Africa&apos;s Pioneer Voiceover Conference</p>
        <button className='register_btn py-3 w-25 border-0 rounded-2 ft text-white orange'>Register</button>
      </div>
      
    </div>
  )
}

export default DoAction
