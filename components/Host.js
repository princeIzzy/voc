import Image from "next/image";
import { AiOutlineClockCircle } from "react-icons/ai";

const Host = ({Img,speech,detail,duration,display}) => {
    return(
        <div className=' border border-1 shadow d-md-flex px-0 rounded'>
            <div className=' bg-black d-flex justify-content-center w-md-75'>
                <Image src={Img} alt='speaker' width={230} height={240} className='rounded-md-start '/>
            </div>
            <div className=' d-flex flex-column w-100 h-auto p-3 px-md-5 py-md-4 bg-black rounded-end justify-content-between '>
                <div className='text-white'>
                    <h6 className='clash_light  '> 
                        <span className='ft me-3 fw-light p-2 lightOrage'>{speech}</span>
                    </h6>
                    <p className=' mb-3 fw-bolder ft-3 pe-md-5'>{detail}</p>
                </div>
                <div className={`text-white raleway ${display}`}>
                    <div className=' cu-gray p-3'>
                    <span className='me-2'><AiOutlineClockCircle/></span>
                    {duration}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Host;