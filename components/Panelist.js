import Image from "next/image";


const Panelist = ({Imgesrc, name,role, sign, preview,location}) => {
    return (
        <div className='border border-1 bg-black shadow-sm d-md-flex px-0 rounded overflow-hidden'>
            <div className=' pt-5 pt-md-0 d-flex justify-content-center' data-aos="fade-zoom-in">
                <Image src={Imgesrc} alt='panelist image' width={230}  className='rounded-start h-100' />
            </div>
            <div className='cover d-flex flex-column h-auto p-3 px-md-5 py-md-4 justify-content-between'>
                <div className="d-flex align-items-center">
                    <h5 className='ralewayExbold fw-bold text-white m-0 ft' >{name}</h5>
                    <div className='ms-3 ms-md-none px-2 py-1 ft-1 lightOrage'> {role}</div>
                </div>
                <div className='pt-2 mb-3 mb-md-0'>
                    <p className='mb-0 ft-1 text-white raleway'>{preview}</p>
                </div>
                <div className='text-white mt-3 d-flex raleway'>
                    <div className=' cu-gray px-3 py-2'>
                        <span className='pe-2'>{sign}</span>
                        {location}
                    </div>
                </div>
            </div>
        </div>
    )
};
export default Panelist