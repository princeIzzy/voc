import Image from "next/image";

const SpeakerInfo = ({ imageSrc, name, role }) => {
    return (
      <div className='slide bg-white mx-3 my-3 rounded-4 align-items-center p-4 d-flex'>
        <Image src={imageSrc} width={60} height={60} alt='featured speaker' className='rounded' />
        <div className='ps-3 d-flex flex-column justify-contents-center'>
          <h3 className='fs-6 fw-normal text-nowrap mb-0'>{name}</h3>
          <p className='mb-0 ft'>{role}</p>
        </div>
      </div>
    );
};
export default SpeakerInfo
  