import React, { useState } from 'react';

const Subscribe = () => {
  const [email, setEmail] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [submissionStatus, setSubmissionStatus] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsSubmitting(true);

    try {
      const response = await fetch('/api/newsletter', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });

      if (response.ok) {
        setEmail('');
        setSubmissionStatus('success');
        console.log('Email subscribed successfully!');
      } else {
        const responseData = await response.json();
        if (response.status === 400 && responseData.error === 'Email already subscribed') {
          setSubmissionStatus('exists');
          console.error('Email already subscribed');
        } else {
          setSubmissionStatus('failure');
          console.error('Failed to submit email:', responseData.error || 'Unknown error');
        }
      }
    } catch (error) {
      setSubmissionStatus('failure');
      console.error('Error submitting email:', error);
    }

    setIsSubmitting(false);

    // Reset status after 5 seconds
    setTimeout(() => {
      setSubmissionStatus(null);
    }, 5000);
  };

  return (
    <div>
      <h5 className='text-white'>Subscribe</h5>
      <p className='ft opas text-white'>To our Newsletter - info@thevoiceoverconference.com</p>
      <form onSubmit={handleSubmit} className='py-md-2 px-0 d-block d-md-flex'>
        <input
          type="email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
          placeholder='jane@gmail.com'
          className='border-0 col-12 col-md-8 px-3 py-2 mb-3 mb-md-0 footer-input text-white ft'
        />  
        <button
          type="submit"
          className='border-0 bg-light ft px-3 py-2 col-12 col-md-4 fw-semibold text-black'
          disabled={isSubmitting}
        >
          {isSubmitting ? 'Sending...' : (
            submissionStatus === 'success' ? 'Sent' :
            submissionStatus === 'exists' ? 'Already subscribed' :
            submissionStatus === 'failure' ? 'Failed' : 'Submit'
          )}
        </button>
      </form>
    </div>
  );
};

export default Subscribe;
