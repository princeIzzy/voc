import React from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { newlogo2 } from '@/_assets/images'
import { BsFacebook, BsInstagram, BsLinkedin, } from 'react-icons/bs';
import Subscribe from './Subscribe';

const Footer = () => {
  return (
    <div className=' py-5 cu-gray footer-cont '>
      <div className=' container '>
        <div className='row d-flex'>
          <div className='col-12 my-5 col-md-7 col-lg-8'>
            <Link href='/' ><Image src={newlogo2} width={150} alt='Logo' className='mb-4' /></Link>
            <ul className='d-flex list-unstyled opas col-12 col-md-8 col-lg-6 d-flex justify-content-between text-white'>
                <Link href='/about' className='text-decoration-none text-white'><li className='ft'>About</li></Link>
                <Link href='/contact' className='text-decoration-none text-white'><li className='ft'>Contact</li></Link>
                <Link href='/speakers' className='text-decoration-none text-white'><li className='ft'>Speakers</li></Link>
                <Link href='/schedule' className='text-decoration-none text-white'><li className='ft'> Schedule</li></Link>
            </ul>
          </div>
          <div className='col-12 col-md-5 col-lg-4 mb-5 py-5 px-md-4 sub_cont rounded '>
            <Subscribe/>
          </div>
        </div>
      </div>
      <div className='bg-white footer-line w-100 opacity-25 '></div>
      <div className='container pt-5 d-block d-md-flex justify-content-between'>
        <p className='ft text-white  opas'>Powered by © waystream.io</p>
        <div className=' d-flex col-4 col-md-2  justify-content-between'>
          <a href='https://www.facebook.com/thevoiceoverconference/' > <BsFacebook  className='  text-white'/> </a>
          <a href='https://www.linkedin.com/in/the-voiceover-conference-264307271/' > <BsLinkedin  className='  text-white'/> </a>
          <a href='https://www.instagram.com/thevoiceoverconference/' > <BsInstagram  className='  text-white'/> </a>
        </div>
      </div>
    </div>
  )
}

export default Footer
