'use client'
import React from 'react'
import Image from 'next/image'
import Link from 'next/link';
import {newlogo1, downarrow} from '../src/_assets/images'
import {RiMenu3Fill} from 'react-icons/ri'
import { useEffect } from 'react'
import { useRouter } from 'next/router';

const Header = () => {
  useEffect(()=> {
    if(typeof window !== "undefined"){
      require('bootstrap/dist/js/bootstrap.js')
    }
  }, []);

  const router = useRouter()
  return (
    <div className='bg-white position-sticky top-0 z-3'>
      <nav className='container px-0 '>
        <div className='py-3'>
          <div className='d-flex justify-content-between align-items-center'>
            <div className='col-5 '>
              <Link href="/">
                <Image src={newlogo1} alt='Logo'  className='w-auto'/>
              </Link>
            </div>

            {/* mobile */}
            <button className="btn cu-orange  d-block d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
              <RiMenu3Fill />
            </button>

            <div className="offcanvas offcanvas-start w-75"  id="offcanvasRight" aria-labelledby="offcanvasRightLabel" >
              <div className="offcanvas-header">
                <h5 className="offcanvas-title" id="offcanvasRightLabel">
                  <div className='col-5 ' data-bs-dismiss="offcanvas">
                    <Link href="/">
                      <Image src={newlogo1}  alt='Logo' />
                    </Link>
                  </div>
                </h5>
                <button type="button" className="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
              <div className="offcanvas-body">
              <ul className=' fw-light align-items-centers list-unstyled'>
                <li className='py-2 px-auto ft' data-bs-dismiss="offcanvas"> 
                  <Link className='text-decoration-none text-black' href='/about'>About</Link>
                </li>
                <li className='py-2 px-auto ft' data-bs-dismiss="offcanvas"> 
                  <Link className='text-decoration-none text-black' href='/speakers'>Speakers</Link>
                </li>
                <li className='py-2 px-auto ft' data-bs-dismiss="offcanvas"> 
                  <Link className='text-decoration-none text-black' href='/schedule'> Schedule</Link>
                </li>
                <li className='py-2 dropdown px-auto '> 
                    <div className="accordion">
                      <button className="accordion-button p-0 bg-white text-black fw-light ft" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" >
                        Gallery
                        </button>
                      <div id="collapseOne" className="accordion-collapse border border-0 collapse " data-bs-parent="#accordionExample">
                        <div className="accordion-body py-1 px-0">
                          <div className="accordion-item border-0 py-2" data-bs-dismiss="offcanvas"><Link className='text-decoration-none text-black' href='/gallery/2023'>Year 2023</Link></div>
                          <div className="accordion-item border-0 py-2" data-bs-dismiss="offcanvas"><Link className='text-decoration-none text-black' href='/gallery/2022'>Year 2022</Link></div>
                        </div>
                      </div>
                    </div>
                </li>
                <li className='py-2 px-auto ft' data-bs-dismiss="offcanvas"> 
                  <Link className='text-decoration-none text-black' href='/blog'>Blog</Link>
                </li>
                <li className='py-2 px-auto ft' data-bs-dismiss="offcanvas"> 
                  <Link className='text-decoration-none text-black' href='/contact'>Contact</Link>
                </li>
                <Link className='text-decoration-none' href='/register'> <li className='register_btn orange py-2 px-4 rounded text-white text-center ft' data-bs-dismiss="offcanvas">Register</li></Link>
              </ul>
              </div>
            </div>

            {/* destop view */}
            <div className='col-7 d-none d-lg-block'>
              <ul className='d-flex  justify-content-between mb-0 fw-light align-items-centers list-unstyled'>
                <li className='py-2 px-auto ft'> 
                  <Link className='text-decoration-none text-black' href='/about'>About</Link>
                </li>
                <li className='py-2 px-auto ft'> 
                  <Link className='text-decoration-none text-black' href='/speakers'>Speakers</Link>
                </li>
                <li className='py-2 px-auto ft'> 
                  <Link className='text-decoration-none text-black' href='/schedule'> Schedule</Link>
                </li>
                <li className='py-2 dropdown px-auto ft'> 
                  <a className=" dropdown-toggle text-decoration-none text-black" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Gallery
                    <Image src={downarrow} alt='down arrow' className='ms-3' />
                  </a>

                  <ul className="dropdown-menu ">
                    <li className="dropdown-item"><Link className='text-decoration-none text-black' href='/gallery/2024'>Year 2024</Link></li>
                    <li className="dropdown-item"><Link className='text-decoration-none text-black' href='/gallery/2023'>Year 2023</Link></li>
                    <li className="dropdown-item"><Link className='text-decoration-none text-black' href='/gallery/2022'>Year 2022</Link></li>
                  </ul>
                </li>
                <li className='py-2 px-auto ft'> 
                  <Link className='text-decoration-none text-black' href='/blog'>Blog</Link>
                </li>
                <li className='py-2 px-auto ft'> 
                  <Link className='text-decoration-none text-black' href='/contact'>Contact</Link>
                </li>
                <button onClick={() => router.push('/register')} className='register_btn border-0 orange py-2 px-4 rounded text-white ft'><a>Register</a></button>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default Header
