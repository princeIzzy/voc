// MorePosts.js
// 'use client'
import React, { useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';

const MorePosts = ({ otherPosts }) => {
    return (
        <section className='container px-md-0'>
            <h3 className='mb-3'>More Posts</h3>
            <div className='row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center g-4 title_space'>
                {otherPosts.map((otherPost) => {
                    return (
                        <Link key={otherPosts.slug} href='/blog/[slug]' as={`/blog/${otherPost.slug}`} passHref className='text-decoration-none text-black'>
                            <div className='col'>
                                <div className='card shadow rounded'>
                                    <Image src={otherPost.cover_image} width={500} height={200} className='card-img-top w-100 ' alt="cover image" />
                                    <div className='card-body p-3'>
                                        <h4 className='card-title pe-md-3'>{otherPost.title}</h4>
                                        <div style={{height: 60 + 'px' }}className='overflow-hidden my-4'>
                                            <p className='card-text opas  ft-1'>{otherPost.subtitle}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    );
                })}
            </div>
        </section>
    );
};

export default MorePosts;
