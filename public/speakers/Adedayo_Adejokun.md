---
name: Adedayo Adejokun
location: Nigeria
preview: Enterprise communicator, Voiceover Artist, Coach
image: /speakers/img/Adedayo_Adejokun.jpg
order: 40
---

Adedayo isn't just an enterprise communicator; she's a multifaceted personality who uses her voice to empower, educate, and inspire. With over 17 years of experience, she seamlessly navigates the realms of brand storytelling, strategic business development, and captivating voice artistry. Her career began with crafting compelling narratives for tech companies. This experience transitioned into the dynamic world of FinTech, where she honed her skills in understanding audiences and crafting impactful messaging.
Her captivating voice has delivered powerful messages for leading brands like GlaxoSmithKline and Access Corporation, while also uplifting non-profit initiatives like Society for Family Health, House of Freedom, and the Mental Care Foundation. Her talent has garnered recognition, with a nomination for the prestigious One Voice UK Award in the category of Best Female International Voice Artist (2022).

Adedayo isn't just a gifted voice-over artist; she's a skilled on-camera host. She previously captivated audiences as a co-host of the gospel show, Uplift Africa, on RAVE TV. Currently, she helms "The Family and Friends Conversations," a platform that allows her to delve into critical social issues confronting families, such as mental health, drug abuse, financial planning, parenting, and marriage, fostering open and insightful conversations.

Adedayo's dedication extends beyond the microphone. She's a facilitator at the Voiceover Academy, Africa's pioneer training hub and community for voice actors, where she generously shares her expertise to empower others. Further amplifying her commitment to the industry, she served as the President of the Voiceover Academy Alumni, Africa's largest community of voice talents, in both 2020 and 2021. A champion for women's empowerment, this professional mistress of ceremonies has hosted numerous high-profile corporate events both locally and internationally, including the Abuja Accelerator event in New York and the Foreign Investment Network and Forbes Africa Awards in Dubai.
Whether it's crafting strategic communication or leading insightful discussions, Adedayo's voice leaves a lasting impact. She believes that the voice isn't just a tool for communication, it is a catalyst for creating positive change.