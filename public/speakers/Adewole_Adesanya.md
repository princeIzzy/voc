---
name: Adewole Adesanya
location: Nigeria
preview: Music Producer, Sound Engineer, Musicologist
image: /speakers/img/Adewole_Adesanya.jpg
order: 16
---

ADEWOLE ADESANYA popularly known as Mr. Wols is from Ogun state, Nigeria. He is the lead pastor at Revealing Christ International Mission. He is also the founder & Creative Director of Blue Flame Media. Mr. Wols is one of the founding members of The Vessel. He holds a Music degree from the University of Lagos.
His journey with Music started early, he learnt to play the keyboard at age 13 and he became the assistant organist in his grandfather's church at age 14.

He is a multiple award music producer, Musicologist, praise and worship leader, music instructor, Pianist, Guitarist, and has worked with, produced, mixed and mastered songs for notable artistes and brands like TY Bello, Lara George, Kent-Edunjobi, Emma oh ma God, Dunsin Oyekan, Nosa, David Nkennor, Victoria Orenze, Freke Umoh, Tolu Ijogun, Olumide Iyun, K'ore, Pastor Wale Adenuga, Infinity, Mike Abdul, Panam Percy Paul, Glowreeyah Braimah,  AMVCA, Sky Bank, Hollandia, FCMB, amongst many others. 

He studied the Art of Mixing at Berklee's online college of music. 
He lives with his family in Lagos, Nigeria. He's married to Iniobong Adesanya. Their union is blessed with three children.
