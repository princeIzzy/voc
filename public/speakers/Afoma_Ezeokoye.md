---
name: Afoma M. Ezeokoye
location: Nigeria
preview: Lawyer, storyteller, voiceover artist
image: /speakers/img/Omaha.jpg
order: 39
---

Afoma M. Ezeokoye (nee Chiegboka), popularly known as “Oma the Storyteller," is a lawyer, storyteller, human rights advocate, and award-winning voiceover artist with over 11 years of experience. Oma has written, voiced and directed documentaries and short films to help several local & international non-profit organisations and brands bring their visions to life, including the World Bank Group, The Hunger Project, UN's International Organisation for Migration, Transparency International Canada, etc. Her storytelling works have won prestigious international awards such as the 2023 Voice Arts Award by the Society of Voice Arts and Sciences (SOVAS), the 2023 FUGAZ Award for Best Documentary Short Film, the UNWTO Inspiration Africa Branding Challenge Award 2020, etc.

Oma is also the COO and co-founder of Raknida, an arttech platform that empowers black, African, and other underrepresented digital artists to earn passive income through their artworks. In addition, she is the Director of Communications and Operations at Insecta Studios, a creative services company dedicated to visual storytelling. In her various positions, Oma has managed creative and digital product development projects for businesses including Jobberman, Omnibiz, Moniepoint, Interswitch, Contec Global, Petrogas Energy, etc. She also developed Raknida’s MVP e-commerce website from scratch on a no-code platform. 

Oma is an alumnus of the Carrington Youth Fellowship Initiative (CYFI), a Co-Founder of BoyUP (Boys Under Protection), an initiative that seeks to protect the boy child from sexual abuse and its effects, and the screenwriter and director of  'Surviving: When Boys Get Sexually Abused', a short documentary film by BoyUP. In 2023, she led the planning committee for the annual Ambassador Walter Carrington Symposium (AWCS), and they successfully pulled off the biggest symposium ever organised in the three-year history of AWCS.

Her goal is to inspire people around the world with authentic African stories.
