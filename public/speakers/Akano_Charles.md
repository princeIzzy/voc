---
name: Akano Charles
location: Nigeria
preview: Voice Actor, Radio Presenter, Spoken Word Poet
image: /speakers/img/Akano_Charles.jpg
order: 31
---

Akano Charles Jnr, Professionally known as Ace Wondaz is a radio presenter, Spoken word poet, podcaster, Voice actor and a multi-Award winning Voiceover artist. He is the second runner-up in the Maiden edition of the Nigerian Voice Acting Contest 2021 organized by one of the leading voice-over training institutes, Voiceover Workshop. He is also, a voice talent coach and an amazing voice talent using his voice to help brand businesses and organizations. He is the Host of the Comnerdic Podcast, a podcast that focuses on Highlighting graphic novels/comic books and animations especially from Nigeria. He is a storyteller and is passionate about sharing experiences. He is what you would call a creative by trade.