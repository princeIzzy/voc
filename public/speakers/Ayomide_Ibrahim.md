---
name: Ayomide Ibrahim
location: USA
preview: CEO, FinServe Pro, Financial Expert, Mentor
image: /speakers/img/Ayomide_Ibrahim.jpg
order: 11
---

Ayomide Ibrahim is an astute entrepreneur cum professional accountant and the CEO of FinServe Pro, a financial services company providing accounting, tax, loan, and insurance services to individuals and organizations alike.

FinServe Pro is located in Maryland and Texas USA. 

Ayomide is a certified accountant in 3 different countries. He is a Fellow of the Institute of Chartered Accountants of Nigeria, ICAN; an Associate Member, of the Institute of Chartered Accountants of England and Wales, and a Certified Fraud Examiner. 
He is also a Certified Information System Auditor (CISA) and Certified Information Security Manager (CISM).

Before moving to the United States in 2017, Ayomide served with the Lagos State Government as an accountant (2006 – 2017). 

In 2016, Ibrahim Co-founded Sept30 Media and Productions, where he also worked as the CFO.
In 2018, he joined Sharf Pointers Financial Services, a CPA firm in Maryland, and later Robert Half where he worked as a Consultant (Accounting & IT Audit and Tax) in several companies and firms including PwC, Five Guys Enterprise, and now Smart Electric Power Alliance in Washington, DC.

In 2022, he was awarded the AD King Young Professionals’ Iconic Award at the AD King Young Professional Boot Camp for his unflinching commitment to creating financial inclusion for all.
