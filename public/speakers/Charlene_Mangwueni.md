---
name: Charlene Mangwueni-Furusa
location: Nigeria
preview: Voiceover Artist, Content Creator, Coach
image: /speakers/img/Charlene_Mangwueni.jpg
order: 20
---
Charlene Mangwueni-Furusa is an outstanding screen actor, voiceover artist and broadcaster based in Zimbabwe Over the years, she has had an outstanding career as both a performer and voice acting professional with numerous brands and accolades attached to her name.

2023 – WINNER LADIES IN MEDIA AFRICAN FEMALE VOICE ARTIST OF THE YEAR

2018 WINNER- 3RD EDITION BULAWAYO ARTS AWARDS- OUTSTANDING THEATRE ACTRESS FOR UKAMA https://www.musicinafrica.net/magazine/bulawayo-arts-awards-2019-all-winners

2018 WINNER-17TH EDITION NAMA OUTSTANDING THEATRE ACTRESS FOR THE INCIDENT http://www.chronicle.co.zw/actress-looks-beyond-borders/

2017 -WINNER – ALPHA MEDIA HOLDINGS 100GREAT ZIMBABWEANS AWARD, MEDIA AND FILM CATEGORY

2017 - MY 2017 FILM COOK OFF, IS THE FIRST ZIMBABWEAN FILM TO BE ON NETFLIX AND EARN GLOBAL AUDIENCE AND HAS WON NUMEROUS AWARDS GLOBALLY.
