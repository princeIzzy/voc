---
name: Charles Alade
location: Nigeria
preview: Founder, Voice Over Bootcamp, Media Personality, Voiceover Coach
image: /speakers/img/Charles_Alade.jpg
order: 28
---

Charles Alade is a communications strategist, media and advertising executive. He is the immediate past General Manager of Royal Roots 92.9FM Ibadan and the Team Lead at Hebbymix Studios Limited, a multimedia company that offers media solutions to businesses around the world. A member of Forbes Black Community and an Associate Member of The Advertising Regulatory council of Nigeria (ARCON).
He is a renowned broadcaster, talent manager, and conference speaker. In addition, he is an international voice artist and coach. His body of work as a media executive includes hosting a variety of programs on radio stations like Premier FM, Kings FM, Star FM, and Solutions FM on topical issues around culture/lifestyle, business, and entertainment. He is a multi-instrumentalist and musician who has performed and recorded many popular songs, such as “Alapanla” “Caesar,” and “Special To Me.” He has won numerous recognitions, such as the “Nigeria Broadcasters Merit Award Team Song Winner 2018,” and “Create Naija Entertainment Awards 2022.” A two time Most Impactful Media Personality in Nigeria Nomination by Naira Awards.
His voice is used in a wide variety of productions, including commercials, audio books, cooperate videos, and promos for many prestigious companies such as MTN Nigeria, itel, Chi exotic, Mutual Trust microfinance bank, Dominican university, Foodco Nigeria, Westgate lifecare Mall, etc.
He is the founder of the Voiceover Bootcamp Africa, a platform for training and exporting professional voice actors, often hosted in collaboration with veteran BBC broadcaster Peter Baker and other international facilitators around the world and has trained over 400 voice over talents. Charles’s literary prowess is also evident with his best-selling book “Overnight Success.”