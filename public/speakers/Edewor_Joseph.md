---
name: Edewor Joseph Ajueshi
location: Nigeria
preview: Media Personality, Event Compere, Actor, Voiceover Artist
image: /speakers/img/Edewor_Joseph.jpg
order: 38
---
Edewor Joseph Ajueshi, popularly known as VoiceofGAD, is a distinguished media personality, event compere , stage and screen actor and an award-winning voice-over actiste, renowned for his extensive portfolio of projects. Currently serving as the Lead Entertainment Producer and Anchor at New Central TV, one of Africa's largest media organisation, Edewor boasts over 10 years of media experience. He has collaborated with some of the continent's most prominent media giants.
As a voice actor, Edewor has lent his talents to major brands in Nigeria and beyond, producing professional radio jingles, brand promotional videos, audiobooks, TV adverts, and more. Additionally, he is a social media influencer and content creator making a significant impact both online and offline.
Edewor is also the CEO of FarelyUsedNG, an online decluttering brand, and the co-founder of FreshWolf Casuals, a fashion casual brand. A patriotic Nigerian, he is committed to creating a better society, starting with his immediate environment.
