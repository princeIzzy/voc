---
name: Ejiro Kerewi
location: Nigeria
preview: Radio/TV presenter, Red Carpet Host, Voiceover Artist
image: /speakers/img/Ejiro_Kerewi.jpg
order: 35
---

An experienced Radio and Tv presenter with a passion for giving listeners an enjoyable and engaging experience with over 5 years of working experience hosting corporate and social events, presenting TV and radio shows, announcing program schedules, interviewing high profile guests, producing talk shows, reporting news, and recording voice over jingles.