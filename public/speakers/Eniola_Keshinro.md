---
name: Eniola Keshinro
location: Nigeria
preview: Voice Actor, Coach, Event Host
image: /speakers/img/Eniola_Keshinro.jpg
order: 25
---

Eniola Keshinro, is a multi-talented individual. As a professional Voiceover artist, Eni K. is a versatile performer who has earned a reputation for her trustworthy and natural conversational style; which continues to help audiences connect with solutions. She has voiced projects across VO genres for both local and international clients. Sought after by premium brands, Eni K specializes in telling captivating stories and building trust through her voiceover delivery. 

You can hear Eniola on commercials and promos on TV, Radio, Internet and other channels, she can also be heard on IVR and e-learning courses for a number of institutions. Her background and decade experience in the education industry has led her to being the preferred voice for e-learning and corporate narrations. 

As a multi-hyphenate, Eni K. embodies authenticity and fully explores different God-given gifts and talents. She encourages the same with her growing audience. Eniola teaches Storytelling at the Voiceover Academy, Nigeria and serves as the President of the Alumni Network, where she fosters collaboration and contributes to the growth of the Nigerian Voice Over Industry, through creative initiatives.

She is an award winning Commercial Voiceover artist and also a 3x Voice Arts Awards Nominee in the TV and Radio Commercial African category. 

Outside of voiceovers, Eniola is also a Fashion Entrepreneur and is currently building Krenitive Style, a contemporary Womenswear Brand with pieces made in Nigeria for women across the world.
