---
name: Esther Kemi Gbadamosi
location: Nigeria
preview: Founder/Creative Director, Radioxity Media, Writer, Cinematographer
image: /speakers/img/Esther_Kemi.jpg
order: 19
---

Esther Kemi Gbadamosi is the Founder/Creative Director of Radioxity Media. A writer, cinematographer, editor and stop motion animator.
Her films have received several international filmmaking awards including Berlinale Mastercard Fellowship 2024, Berlinale Talents 2023, Winner MIFA Annecy 2022 Nigeria, selected for Animarkt Workshop 2023, Best Animation at the Inshort Film Festival 2021 in Lagos, Best Stop motion Animation LIFANIMA 2022, the Independent Short Awards Los Angeles for the Best Female Director, the Best Documentary at the Lake International Pan African Film Festival Kenya, Nominated for Best Animation at AFRIFF and AMAA Awards 2022 respectively.

She was also selected by the Durban FilmMart 2022 and Cape Town International Animation Festival 2022 to present her upcoming animated film 'Prepared to Die'. She was recently listed
on the MIPTV Cannes Producers to watch 2023.