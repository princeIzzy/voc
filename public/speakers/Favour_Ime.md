---
name: Favour Ime
location: Nigeria
preview: Voice Actor, Legal Practitioner 
image: /speakers/img/Favour.jpg
order: 37
---

Favour Ime is a lawyer who currently serves as the Senior Regional Manager for Africa at Open Ownership - an international NGO dedicated to enhancing corporate transparency to combat corruption and illicit financial flows. In her role, Favour leads technical support initiatives for national governments across Africa, guiding them in their commitment to corporate transparency.

Favour frequently represents her organisation on national, regional, and international platforms, where she excels in leading panels and delivering impactful presentations to a wide range of audiences. Favour has a keen interest in public speaking, a skill that is integral to her career and she has effectively integrated voice acting into her professional repertoire.

Since 2020, Favour has honed her voice-over skills through rigorous training at the VoiceOver workshop and personalized coaching sessions with the renowned Mr. Emmanuel - King E. Her voice-over portfolio includes successful collaborations with notable clients such as Aura by Transcorp Hilton, showcasing her versatility and growing expertise in the field.

Beyond her professional achievements, Favour is an avid music enthusiast.
