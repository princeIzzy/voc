---
name: Folu Storms
location: Nigeria
preview: Actor, Voiceover Artist, T.V./Event Host
image: /speakers/img/folu.jpg
order: 13
---
Folu Storms is a versatile force in the entertainment industry, seamlessly blending her roles as an accomplished actor, media personality, and voice performer. Graduating from the prestigious
Relativity Media Film School, Folu has not only honed her craft but has elevated it to new heights. Her captivating presence has graced the screen in award-winning productions such as "Crime and Justice Lagos," "The Men's Club," and "Unmarried," establishing her as a luminary in the world of acting.

Beyond her acting prowess, Folu Storms is a captivating orator whose words resonate with power and authenticity. Her ability to command attention is not confined to the screen; she effortlessly captivates audiences across various platforms. A skilled dancer as well, she infuses her performances with a natural grace that adds an extra layer of depth to her artistic expression. 

Folu Storms is more than an actor; she is a conditioned athlete, embodying resilience and dedication in her pursuit of excellence.  With a unique blend of talent, charisma, and physical prowess, Folu Storms continues to leave an indelible mark on the entertainment landscape, captivating hearts and minds wherever her artistry takes her.