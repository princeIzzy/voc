---
name: Gboyega Akosile
location: Nigeria
preview: Special Adviser, Media and Publicity to Lagos State Governor, Mr. Babajide Sanwo-Olu.
image: /speakers/img/Gboyega_Akosile.jpg
order: 0
---

Gboyega Akosile is a communication and media strategist. After the successful completion of his primary and post primary education, Akosile proceeded to the University of Ilorin, Kwara State where he graduated with a Bachelor of Arts (Hons) in Performing Arts and later Master of Arts degree (M.A, Theater Arts) from the University of Lagos. 

Akosile discovered his passion for public service and media early, starting off from his National Youth Service Corps days as the Artistic Director, NYSC, Lagos Chapter in 1996. On passing out from the National Youth Service Corps scheme, he joined the Lagos State Television as a reporter and his style and flair opened an opportunity for him to become a producer as well. His quest for modern news techniques made him to take up a role as a reporter, producer and presenter at Channels Television, Lagos, where he was involved in the daily news production as well as presentation of two in-house programs.
 
Service to Lagos state beckoned again and he returned to the State owned Lagos State Television as a Senior Producer in 2002, in consultancy capacity. 
 
In 2004, he took up a bigger role as Head of News at MITV, where he worked assiduously to place the news program of the station on the national stage.
 
Akosile later joined the newly recapitalized Minaj Broadcast International (MBI) as one of the few staff that turned around the fortunes of the station. He was appointed as political editor and later doubled as Head of Reportorial corps of the Television station. 
 
A prolific writer and political watcher, Akosile has contributed several opinion articles on the state of the nation for different national newspapers. 
 
A quest for further knowledge led Akosile to the Obafemi Awolowo University, Ile-Ife where he bagged a Master’s degree in Business Administration (MBA) in 2008.
 
With a career spanning over a dozen years, Gboyega Akosile decided to set up his own Media and Communications consultancy outfit, Bridgeworld Communications Company where he worked both in managerial and administrative capacities as the Chief Executive Officer and Executive Editor of some of its news titles.
 
Akosile, a lover of the Arts, has been involved in several theatre projects for the  development of the humankind.

He is a member of the Nigerian Institute of Public Relations, Nigeria Union of Journalists, Fellow of the Institute of Strategic Management of Nigeria and the National Association of Nigerian Theatre Arts Practitioners (NANTAP), among other professional bodies.

Akosile is married and blessed with children.
