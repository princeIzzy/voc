---
name: Kamiza Chikula
location: Zambia
preview: CEO, Feira Communications, Voiceover Artist
image: /speakers/img/Kamiza_Chikula.jpg
order: 14
---

Kamiza Chikula is a consummate communicator with nearly three decades of experience in  every aspect of Marketing, Public Relations and Corporate Communications.  Kamiza has held numerous high profile positions in several international organisations such as  MultiChoice Zambia, Habitat for Humanity Zambia, Barclays Bank Zambia, Standard Chartered Bank Zambia and in his last formal job was employed as Head of Marketing and Communications at Stanbic Bank Zambia Limited. In 2015, Kamiza established Feira Communications (Feira Comms), a company offering bespoke Corporate Communications services to a wide array of organisations. 

Kamiza has held his own hosting numerous State functions, high profile corporate events, the Zambia Tourism Awards, Public Speaking and Communications workshops for both Government and corporate clients, as well as the MTN Group Y'ello Star Awards alongside Jo-Ann Strauss at the Johannesburg City Hall. 

Kamiza has hosted every Stanbic Music Festival and events carried live on national television. 

Kamiza produced, directed and presented Investrust Bank's InvestChat Radio Show, the MultiChoice Zambia Subscribe and Win television show, hosted the Enterprising Zambia with Barclays television show and had several guest presenter stints on Standard Bank's Blue Wave produced by JLP Television. On the Voice Over front, Kamiza has done work across the region with many respected media institutions and is managed internationally by Intertalent (Pty) Limited. Cases in point are the DStv Extra Mile advertisements, HSBC World Rugby Sevens Series promotional advertisements, GOtv advertisements, the Zambia Tourism Agency AU Summit 
promotional video, the Zambia Development Agency "Invest in Zambia" promotional video, and the SMART Zambia Interactive Voice Recordings used on their automated call centre system. 

Kamiza enjoys giving talks and has spoken to audiences at the YALI Regional Conference (Johannesburg), the US Embassy American Centre, The American Corner (NIPA, Lusaka), Chikumbuso Project, Presbyterian Church, Miracle Life Family Church, Ladies Circle Zambia, Zambian political parties Inter-Party Media Teams Forum convened by the National Democratic Institute, the Government Communication Strategy workshop (Siavonga), and at several companies in Lusaka where he also conducted tailored sessions to suit their specific requirements. Kamiza regularly speaks on Effective Communication, Personal and Corporate Brand Enhancement, Fulfilling Purpose and Handling the Media. Kamiza works well speaking off the cuff and is never daunted by sudden changes in programmes or scripts. A very quick learner, Kamiza embraces each event, project and assignment with zest and professionalism. The Lusaka native has travelled quite extensively and relishes every opportunity to see and learn new things. 
Kamiza's personal philosophy is "To remain a Champion, Think and Train like a Contender." 
