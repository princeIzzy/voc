---
name: Kay
location: Nigeria
preview: Audio storyteller 
image: /speakers/img/Kay.jpg
order: 33
---

KAY is a poet, audio storyteller, and multimedia journalist who enjoys telling stories about Black love, redemption, personal growth, and delicious romance. 
You can hear her voice and words on “Soft Life Manifestations”, “Love and Shege”, and her recently published audiovisual memoir, “Hope looks good on you”
