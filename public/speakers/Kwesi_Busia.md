---
name: Kwesi Busia
location: Ghana
preview: Voiceveor Artist, Coach, Speed and Drama Coach
image: /speakers/img/Kwesi_Busia.jpg
order: 18
---

Kwesi B. Busia is a British-Ghanaian law graduate and full-time
professional voice actor. Born and raised in the United
Kingdom, Kwesi was exposed to the performing arts from a young age. He frequently participated in theatrical after-school  activities, and was coached in speech, drama, and singing from several institutions.

He began his voice acting career after graduating Summa Cum Laude from university when he was contracted to record a series of audiobooks in Ghana. Since then, his passion for voice acting has grown, and he has garnered over five years of experience working with hundreds of brands in over 27 countries across the globe. He has recorded voiceovers for many different genres, including commercials, documentaries, Pre-learning projects, animations, video games, meditations, and
More. Kwesi’s versatility as a voice actor has allowed him to record  in a range of captivating styles, dynamic tones, and convincing accents, including British and American. He harbors a heartfelt love for the creative process, and thoroughly enjoys bringing scripts to life through his voice acting.
