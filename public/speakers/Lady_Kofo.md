---
name: Lady Kofo
location: Nigeria
preview: Event/Show Host, Voiceover Artist, Media Personality
image: /speakers/img/Kofo.jpg
order: 36
---

The Lady Kofo is a multi-media personality; A Television/Radio Host and Producer, Red Carpet Queen, Commercial and Face Model, Certified Voice Actor, Also a Social and Corporate Event Host .

With almost a decade of reinventing her Brand, The Black Media Girl as she is popularly referred to is leaving no stone unturned in using media as a tool for story telling work African story at the center of it.