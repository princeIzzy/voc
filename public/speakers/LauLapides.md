---
name: Lau Lapides
location: USA
preview: CEO,  Lau Lapides Company, Casting director, Voice Actor, Coach
image: /speakers/img/LauLapides.jpg
order: 8
---

Lau Lapides is the Owner and President of Lau Lapides Company https://laulapidescompany.com/, with hybrid training and production studios in Boston, NYC and LA. She is also the proud pioneer and VO Talent Agent Artist for MCVO, A Division Of Model Club, Inc. in Boston with a voiceover roster showcasing the top talent from coast to coast representing talent from five countries worldwide. MCVO clients from the Regional to National and to International genres in Commercial to Industrial to Animation!
Lau's boutique coaching studios offer top tier training for Voice Over Talent worldwide. Our global coaching team of award-winning broadcasting & media pros are all actively working in the industry and are nationally recognized. We have partnered with the highest level of Talent agencies, managers, casting & producers to provide fresh opportunities to work in major markets for our talent.


Her years-long guest co-host spot on award-winning VO Boss has earned her many international nods, including a SOVAS nomination this past year! She is the co-author of 50 Ways To Mega Crush Your Media Career with former CNN correspondent to the White House Dan Lothian, and a recent contributor to the updated June 2023 edition of James Alburger's book The Art Of Voice Acting. Her studio's Talent Inner Circle (TIC) global acting and voice over members boast some of the top media broadcasting, TV, film and radio artists in the world. As a producer, Her most recent event, New England Voice Over (NEVO), launched the pioneer first ever international voiceover summit and showcase, this past October 2023 just outside of Boston. https://www.newenglandvoiceover.com/, as well as a Virtual Spring Showcase in May 2024! 


She holds an M.F.A. in Acting and is an ArtsBridge Scholar from the University of California at Irvine. As a resident and active member of the thriving New England entertainment industry, she is a proud member of SAG-AFTRA, AEA, Board member of BWME (Boston Women in Media Entertainment), NAB (National Association of Broadcasters), AWART (American Women in Radio & Television), AWM (Alliance for Women in Media), NAPW (National Association of Professional Women) and MBA (Massachusetts Broadcasters Association). 
She was selected as The Ad Club's Women's Leadership Forum Boston *100 Women We Admire* Award, twice a recipient of the prestigious "Women Who Make a Difference Award" honoring entrepreneurial women leaders of the Babson College community. She served as a judge for the National Gracie Broadcasting Awards in Washington D.C. 3 years running and her studio's radio/voice over client biographies were recognized and nominated for a Gracie Award.
