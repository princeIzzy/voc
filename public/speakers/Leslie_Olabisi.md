---
name: Leslie Olabisi 
location: USA
preview: Award-winning & Emmy-nominated VO Artist, Owner Lesile O Voice/LOVStudios
image: /speakers/img/Leslie_Olabisi.jpg
order: 22
---

Leslie Olabisi is a Nigerian American New York Meisner/Method-trained actor, host and voiceover artist, additionally skilled in improv and stage combat, working towards SAFD certification. She launched LOVStudios, a multi-media production company where she hosts several shows on its YouTube Channel and podcast of the same name on Anchor FM, Spotify and additional outlets.

Her 20 yrs in the fashion and entertainment industries developed her extensive skills in design and video editing, specializing in desktop publishing and she offers niche wedding service product, Wedding Magazines.

She is managed by The Talent Express, and represented by Eileen Haves Talent Representatives (Commercial); PN Agency (NYC & Canada voiceover) Devine Voices (int’l VO); Impressive Talent (Mid-Atlantic).
