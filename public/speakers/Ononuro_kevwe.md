---
name: Ononuro kevwe
location: Nigeria
preview: CEO, Kynetics, 2D Animator, Storyteller
image: /speakers/img/Ononuro_kevwe.jpg
order: 27
---

Hungry for growth and burning with passion, Kevwe Ononuro is an exceptional storyteller with keen interest in 2D animation and illustration who hopes to be an integral part of the leading force that changes the existing narrative of storytelling in Africa. Kevwe expresses his creative ability through KHYNETIC, an incorporation he founded in 2016 which he currently heads operations that works to convey compelling tales. He is also building Sonder, a platform dedicated to building a collaborative team of filmmakers and storytellers, along with a community of individuals seeking to gain experience in filmmaking and storytelling. His ambition spills over across several creative roles in film where he is committed to helping individuals, and brands to express their missions, visions and purposes through the creative power of illustration and animation.
