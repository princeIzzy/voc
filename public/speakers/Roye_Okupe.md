---
name: Roye Okupe
location: Nigeria
preview: Award-winning filmmaker, Screenwriter, Founder, YOUNEEK Studios
image: /speakers/img/Roye.jpg
order: 41
---

Born in Lagos, Nigeria, Roye Okupe is an award-winning filmmaker, screenwriter, author, and founder of YouNeek Studios. In 2021, YouNeek landed a 10-book publishing deal through Dark Horse Comics (which was later extended by an additional 20 books in 2021) to create an original line of Superhero and Fantasy stories. From this line, his title IYANU: CHILD OF WONDER was recently greenlit as an animated series at
HBO Max/Cartoon Network, with LionForge Entertainment producing and Roye serving as EP, Director, and Co-Creator. Roye’s accolades include being #5 on Ventures Africa's list of 40 African innovators to watch (2016) as well as being part of NewAfrican Magazines' 100 most influential Africans two years in a row (2016 & 2017). Most significantly, in 2021, Roye was nominated for the Dwayne McDuffie Award for Diversity in Comics, meant to honor "commitment to excellence and inclusion.”
