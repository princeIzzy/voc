---
name: Serah Johnson
location: Nigeria
preview: Director, Tok Studio, Voiceover Artist, Audio Producer
image: /speakers/img/Serah_Johnson.jpg
order: 30
---

Serah Johnson is a Professional and Versatile Voice Talent with over 7 years of experience.
She has worked alongside award winning Audiobook publishing companies and media houses to help breathe life into written word and give them a competitive advantage in the globalspace.

She is the Director of Tok studios a studio that specializes in providing recording services for all kinds of VoiceOver projects and podcasts as well as mixing and mastering and Sound design services.

She is also a Facilitator at the VoiceOver Academy, Africa’s Pioneer school for Voiceover, where she teaches voiceover for Animation and character interpretation.
For her Voiceovers are more than just talking or reading, it’s giving life and expression to written word, helping the end listener feel super connected to a story being told.
