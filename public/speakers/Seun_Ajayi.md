---
name: Seun Ajayi
location: Nigeria
preview: Screen Actor, Voiceover Actor, Coach, Event/Show Host
image: /speakers/img/Seun_Ajayi.jpg
order: 34
---

Seun has been in the business of communication for the past 14 years through the platforms of film, television, theater, radio, and live events by pursuing the disciplines of being an actor, master of ceremonies, and voice-over talent.

 His charismatic personality and smooth baritone voice effortlessly captivate his audiences. His well-known cinematic credits include HUSTLE by Africa Magic, The Lost Okoroshi, and the TV series The Smart Money Woman, among others. Seun also served as the host of the pan-African television program "Confessions," which broadcast on a variety of channels on the Multichoice pay-TV network.

Being a teacher at heart, Seun approaches coaching pupils on the fundamentals of character analysis and emoting for stage and screen audiences in a relatable but effective manner. Over 350 students have received acting instruction from him over the past six years through the Kids Can Act, Voice-over Workshop Pro, and Lufodo Academy of Performing Arts platforms.