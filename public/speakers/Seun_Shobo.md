---
name: Seun Shobo
location: USA
preview: Founder, Voiceover Academy, Voiceover Bank, International Ambassador, SOVAS
image: /speakers/img/Seun_Shobo.jpg
order: 10
---

Seun Shobo is an exceptionally gifted individual, widely recognized as the BrandMASTER. Renowned as a premium voice-over talent, voice-over coach, brand consultant, and inspirational speaker, Shobo embarked on his career as a brand consultant with a fervent commitment to elevating domestic brands to global prominence. Over the years, he has collaborated with numerous multinational brands, contributing both as a distinguished voice talent and a strategic brand consultant. Shobo is credited with the inception of several successful startup brands, effectively guiding them through their growth and expansion phases.
 
With an illustrious career spanning over two decades in the voice-over industry, Shobo's passion for its advancement and his commitment to nurturing African talents for global success led him to establish the pioneer training hub for voice talents in Africa—The VoiceOver Academy which has produced some of Africa's notable voices and content creators. Additionally, Shobo founded The Voice Over Bank, which serves as one of the continent's top rated professional pool of voices and a hub for top-tier audio productions.
 
Shobo's impactful projects have garnered global attention, earning him accolades such as the prestigious Voice Arts ® Outstanding Global Influencer Award by the Society of Voice Arts and Sciences (SOVAS) and the Leadership Impact Legacy Award by the Association of Podcasters and Voice Artists (APVA). As a professional member of the World Voices Organization (WOVO) USA and the Voice and Speech Trainers Association (VASTA), USA, Shobo holds a distinguished position as the first African on the board of VASTA. He is also recognized as an International Ambassador of the Society of Voice Arts and Sciences (SOVAS).
 
A trailblazer in the industry, Shobo authored the groundbreaking workbook "Voice Over Career Advancement Plan" (VOCAP). This practical guide has quickly become a must-have for voice-over talents, offering invaluable insights to help plan their career paths.
 
Through his timeless teachings and insightful nuggets shared on various platforms, Shobo has significantly elevated the appreciation for voiceovers, vocal awareness, and branding among both existing, emerging and multinational brands. He sees himself as part of a generation of creative voices determined to challenge the status quo, spread the voiceover gospel, and contribute to making the world a more excellent voiceover industry.
