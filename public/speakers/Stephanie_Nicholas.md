---
name: Stephanie Nicholas
location: Nigeria
preview: Award Winning Voiceover Artist, Event Host, Content Creator
image: /speakers/img/Stephanie_Nicholas.jpg
order: 24
---

Stephanie Nicholas, popularly known as Voiceofsteph is a seasoned award winning self taught professional voiceover artist, presenter, event host, script writer and content creator based in Lagos State, Nigeria.

She is the winner of the APVA (Association of African Podcasters and Voice Artists) voiceover artist of the year (overall in Africa)”Best Voiceover Artiste” in the Icons Noble Awards 2023.

With over  4 years in the voice over industry, she has trained and coached aspiring voice over talents in the mastery of the art.
 
Founder of the VOSACDEMY, an online voice over and presentation institution based in Lagos Nigeria, she has through the platform trained a total of 172+ students in the VosAcademys 5 Days of Intensive Voice Over Training for Beginners.

Voiceofsteph who became popular on social media through her educative voice over related contents and  for being the voice behind MTN Nigerias customer care rep IVR offers Voice Overs in the British,  American, Nigerian and Pidgin accents, allowing her work with clients from different parts of the world.

Voiceofsteph has delivered top quality Voiceovers for brands like; MTN NG, SAMSUNG, MICROSOFT, FIDELITY BANK, GO MONEY, ZENITH BANK, CAPBRIDGE, CDCARE, TEESAS APP and many more.