---
name: Stephanie Ugbeye
location: Nigeria
preview: Creative Writer, Producer, Singer, Screen & Voice Actor, Content Creator
image: /speakers/img/Stephanie_Ugbeye.jpg
order: 29
---

Stephanie Ugbeye, also known as “The Vocal Expressionist” is a Professional Media Consultant - Creative Writer, Producer, Singer, Screen & Voice Actor, Talk Host and Voice Over Facilitator.
So far, she has worked with over 150 Brands to achieve vocal renditions for various projects with language specifications in English, Itsekiri and Pidgin.
Known for her bright, conversational, engaging, friendly and warm vocal attributes, she is a versatile talent who is able to vocally interpret different characters and expressions by voice acting, singing and narrating for various  voice over projects that include Jingle Productions, Radio & TV Commercials, IVRs, Documentaries, Podcasting, Audiobooks, Animation, Bedtime stories for kids, Radio Dramas and sound effects.
She became a certified Professional and Seasoned Voice Over Talent after completing the Voice Over Pro Course and graduating from the Voice Over Academy in 2017. She also currently facilitates practical voice over learning sessions at the Voice Over Academy, Lagos.
She has also featured on some TV and stage shows which include: Africa Magic’s Mr Grumpy and his little Geniuses where she played the role of a puppeteer and parrot named Pipa. Others include, Africa Magic’s Riona, Visa on Arrival S2 and Showmax’s POAH. In 2021, she had her stage debut performance in the stage play - Otaelo, where she played the role of Chinyere.
She loves to use her voice as a vocal catalyst; one that sells and adds value to brand ideas and concepts through the art of storytelling. This has earned her some reputable recognitions.
In 2019, she was identified by Nigerian Entertainment Detectives as one of the top ten Nigerian female voice over artistes of the decade. In that same year, she won the Voice Over Academy Star Alumni of the year Award. In 2022, she was nominated by SOVAS (Society for Voice Arts and Sciences) for Best Spoken Word Performance and in 2023, she bagged two voice awards for Best Documentary Performance and Best Spoken Word Record in the APVA (African Podcast and Voice Awards).  
Her biggest goal is to crest “The Vocal Expressionist” Brand in the hearts of men and to be remembered as one of the greatest vocal expressionists of all time. 
