---
name: Tolulope Kolade
location: Nigeria
preview: Founder, VOICEVERSE, Voice Actor, Podcaster
image: /speakers/img/Tolulope_Kolade.jpg
order: 21
---

Tolulope Kolade, also known as Tcode is an award-winning Voiceover artist, Voiceover Coach and Podcaster with over 6 years of experience in the VO Industry. He is a former radio host and the winner of Nigeria’s first Voice Acting Contest in 2021, a competition that featured over 60 participants from across the country.

He is the creative partner at the Association of African Podcasters and Voice Actors (APVA), and a member of the Association of Voiceover Artists, Nigeria (AVOA). Tcode is actively involved in organizing, hosting or speaking at Industry events like AVOA webinars, the Voiceover conference, the VO Africa Conference and the APVA Awards.

He is the founder of Coded Voiceovers, VoiceVerseNG, a platform where he and his team amplify Nigerian voiceover artists and their incredible works and the Everything Voiceovers Podcast, where he interviewed voiceover experts across Africa.

