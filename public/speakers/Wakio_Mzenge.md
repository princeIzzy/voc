---
name: Wakio Mzenge
location: Kenya
preview: Screen, Stage & Voice Actor, Film and Theatre Director
image: /speakers/img/Wakio_Mzenge.jpg
order: 15
---

Wakio Mzenge is an award-winning voice, stage and screen actor with vast experience, renowned for her professionalism, craftsmanship and her notable performances in numerous films and TV shows, and on stage. Currently, she plays the lead role in the new Kenyan Showmax Original political thriller series, County 49, where she delivers an electrifying performance as Governor Nerimah Mkung. She has also featured in the Showmax Original police procedural and legal drama Crime and Justice, M-Net’s two-time Kalasha Award winning Swahili telenovela Selina, KTN’s drama series My Two Wives which earned her a Kalasha Award nomination for Best Lead Actress in a TV Drama in 2018, Showmax Original thriller series Igiza, NTV’s Pendo and Citizen TV’s Makutano Junction.
She made her debut film appearance in 2020 with

Her voice has travelled the world in projects such as:
•
Stain Not Shame campaign, which won an award at Cannes 2023.
•
Deutsche Welle’s Learning by Ear series
•
Documentaries by Biscuit Factory and Adastra Development Ltd,
•
Better World Educational videos by Discovery Learning,
•
Women of the World docu-series
•
AMREF - HELP Solution project
•
EAC Infrastructure Documentaries
•
Anzilisha Maternity Health project
•
The voice of Beatrice in the Kiswahili version of 19 Cantos of Dante Alighieri’s Divine Comedy for the initiatives of the MAECI for the 700th anniversary of the demise of Dante Alighieri 2020 courtesy of The Italian Cultural Institute
•
Scripted and voiced the BOMAS at 50 (2021) documentary and numerous radio commercials
•
TVCs and IVRs for brands such as Safaricom, Pwani Oil Ltd, KPLC, NCIC, Nestle, HELB and YU-Mobile.

As a Bachelor of Education holder, Wakio has dedicated her time to teaching performance skills and sharing her vast experience both in virtual and in-person masterclass sessions at KCA UNIVERSITY and Voice Actors League of Kenya (VALK). She was one of the speakers at the 2022 Virtual Voice Over Africa Conference on Swahili as a Language of Business.
Wakio
