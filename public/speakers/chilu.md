---
name: Chilu Lemba
location: South Africa
preview: Author, Award Winning Voiceover Artist, Podcaster
image: /speakers/img/Chilu_Lemba.jpg
order: 3
---

With almost 30 years of experience as a voiceover artist, Chilu has established himself as a highly sought-after professional in the industry. His talent is versatile, ranging from providing intimate narration for company training videos that require staff to wear headphones, to entertaining large crowds at prestigious events such as the Africa Cup of Nations opening ceremony, the Kora All Africa Music Awards, or the CNN/Multichoice Africa Journalist of the Year Awards. Chilu’s distinctive and commanding voice imbues every project he works on with a sense of sincerity and warmth, making him one of the most recognized voices on the continent. He is a prominent figure in the world of radio commercials, television episodes, and public announcements, lending his voice to countless productions across various media platforms. He is a multiple award winning VoiceOver talent.
