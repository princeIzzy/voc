---
name: David Attah
location: Nigeria
preview: Voiceover Artist, Content Creator, Coach
image: /speakers/img/David_Attah.jpg
order: 26
---

David Attah is a passionate voiceover artist, coach and content creator from Nigeria with about 6 years experience in the voiceover field.
He is the founder of Daza Voiceover, a fast growing company that deals with professional voiceover recording, training, mentorship and Management.

With a great sense of humor, he creates, relatable, engaging, entertaining and educative content in form of talking head videos for thousands of followers spread across his social media pages across platforms.

David Attah has recorded Voice Overs for major brands such as DSTV, Fastest Cakes, Obi Cubana's Odogwu Bitters and CFMCG, Development Bank NG, and many others.
He is also an alumni member of the Voiceover Workshop, one of the top Voiceover Institutes in Nigeria.
