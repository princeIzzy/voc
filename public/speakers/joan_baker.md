---
name: Joan Baker Bio
location: USA
preview: COO, SOVAS, Author, Actor, Voiceover Artist
image: /speakers/img/Joan_Baker.jpg
order: 2
---

Joan, an American author, actor, voiceover artist, and on-camera host, serves as the Co-founder, CCO, and Secretary of the Society of Voice Arts and Sciences™ (SOVAS™). Leading this international nonprofit, Joan spearheads the renowned That’s Voiceover!™ Career Expo and the distinguished Voice Arts® Awards, hailed by Good Morning America as the Oscars of Voice Actors. Alongside various initiatives providing invaluable training, education, scholarships, and career counseling for aspiring voice actors, Joan has featured legendary talents such as James Earl Jones, Viola Davis, Mark Hamill, Lily Tomlin, Muhammad Ali, William Shatner, Michael Buffer, Rosario Dawson, Ken Burns, Sigourney Weaver, and Laraine Newman who have graced the stages of the Voice Arts Awards gala.
 
Joan’s teaching credits include Columbia University Graduate Voice Acting Class instructor, sharing her VO expertise with students at New York University, Western Kentucky University, Actors Institute (NYC), and New York’s Professional Performing Arts School, PPAS. As the author of "Secrets of Voice-Over Success," Joan has chronicled the career trajectories of top voice actors, contributing significantly to the understanding of this dynamic field. A recipient of multiple Telly Awards and Promax/BDA Awards, her voice resonates across prominent channels like ESPN, ABC’s The View, HBO, and Grand Theft Auto. On A&E, she graces screens as the risk taking on-camera host for Tribeca Film Festival Red Carpet Interviews.

Beyond her professional accolades, Joan is a philanthropist at heart, featured in notable publications such as The New York Times, Washington Post, and Hollywood Reporter for her commitment to giving back. From her Manhattan studio in the Ed Sullivan Building, she coaches students in the nuances of professional voice acting, from novice to seasoned pro.
 
Joan, a key curator in Spotify's Outside Voices season 3, took center stage at Cannes Lions 2022 alongside journalist Jemele Hill for the panel, 'Representation Behind the Mic: an Outside Voice Event.' Revered for her impactful conversation, Joan's commitment to diversity and amplifying voices in the global creative industry initially garnered the attention of Spotify.
 
Joan became a juror for The Television Arts and Science Emmys in 2022, embracing the opportunity to assess the exceptional work that came across her consideration. Engaging with the artistic brilliance of each submission has become not just a duty, but a source of genuine joy!
 
In late 2022, Joan embraced a new role as an adjunct professor at the Hartt Theatre Division at the University of Hartford, pioneering the institution's inaugural voice acting curriculum.
 
Currently residing in Weehawken, New Jersey, Joan continues to shape the voiceover landscape with her husband and business partner, Rudy Gaskins, embodying a commitment to excellence, education, and philanthropy.
