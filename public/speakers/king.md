---
name: King E Afemikhe
location: Nigeria
preview: Founder, Voiceover Workshop and Media, Convener, The Voiceover Conference
image: /speakers/img/king.jpg
order: 1
---

As a seasoned voiceover artist, producer, and trainer with over 13 years of experience, I work to make a mark in the African voiceover industry.

My journey began as a stage actor and music recording artist, later transitioning to brand and content design. I founded Voiceover Workshop and Media, a leading voiceover training and production institute in Lagos, Nigeria.

Achievements:

- Convener the Pioneer and Biggest Voiceover Conference in Africa, The voiceover conference
- Launched Nigeria's first Voice Acting Contest (now The VOStar)
- Trained hundreds of students across Africa and beyond
- Signed as Brand Ambassador of CalledOut Estates Limited
- APVA award winner for Best Corporate Narration (2022)
- Produced and voiced hundreds of voiceover projects
- Organized numerous webinars, training sessions, one-on-one coaching sessions and conferences.

My goal is to unify and empower African voiceover talents, showcasing them globally while creating platforms for training, recognition, and reward.

I strive to leave a lasting impact on the industry, making a profit while doing so.
Let's connect and explore the possibilities in the voiceover industry together.
