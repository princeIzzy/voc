---
name: Marc Cashman
location: USA
preview: Voice President, Cashman Commercials, Producer, Coach,  Voicever Artist
image: /speakers/img/Marc_Cashman.jpg
order: 12
---

• Voice President of Cashman Commercials. Writing, casting, producing voiceover for Radio & TV or non-broadcast projects.

• Creative Director/Executive Producer with extensive achievements in Radio advertising production, marketing development, budget planning, technology deployment, media promotion and public relations.

• Over 40 years of professional experience in virtually every U.S. market, as well as international markets, in creative development, digital production, casting, writing, directing, editing and performing.

• Internationally recognized speaker to voiceover conventions and conferences, advertising federations and broadcasters associations.

• Recognized for delivering projects on time and on budget through both individual work and the use of small groups and teams.

• Relied upon to design and implement solutions to complex issues that challenge current marketing strategies.

• National and international voiceover instructor and coach. Recognized as one of the top V-O instructors in the U.S. and abroad through classes in "The Cashman Cache of Voiceover Techniques" in Los Angeles, CA, , in-studio and online. Has taught graduate and undergraduate students at USC, UCLA and The California Institute of the Arts. Named one of the "Best Voices of the Year" by AudioFile Magazine three times and an Audie winner.

• Specialties: Radio and TV commercial creation, casting, producing, directing, copywriting, composing (jingles, under-scores), post-production experience.
