---
name: Nadeem Khaled
location: Egypt
preview: Founder & Managing Director Of Evolution , Territory Controller of Gravy For The Brain Arabia.
image: /speakers/img/Nadeem_Khaled.jpg
order: 9
---

Nadeem is a professional voice over artist. He started his journey in the voice over industry in 2010. With the mindset of an architect, Nadeem is able to present creativity and tailored experiences to each of his clients. Those unique experiences and Nadeem’s diligence are the reasons his voice is heard daily on Arabic TV, radio, and social media platforms making him an unmissable part of Arabic contemporary culture. Nadeem continues to share his talents everyday training up-and-coming voice over artists through the international franchise Gravy For The Brain Arabia, that aims to raise investment in the voice over industry with the objective of shaping voice over careers to the market growth’s benefit.