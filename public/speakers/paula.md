---
name: Paula Gammon Wilson
location: Portugal
preview: "(Owner/Director) Pepsqually VO and Sound Design, Inc./Pepsqually Portugal, LDA"
image: /speakers/img/Paula_Gammon.jpg
order: 6
---

Paula Gammon Wilson (Owner/Director)
Pepsqually VO and Sound Design, Inc. (NYC) and Pepsqually Portugal, LDA (Lisbon, Portugal) Paula Gammon Wilson is known for her excellence with managing multinational production teams due to her nearly 30 years of production experience with companies and clients in North America, Asia, Europe, and South America.
Paula is a graduate of Walnut Hill School for the Arts (Design and Production); Pomona College (Theater Production and Movement Studies), the Alvin Ailey School Professional Division (Independent Study); and the New School for Social Research (Screenwriting and 3rd World Cinema).

In 2001, she began working as a union production manager and stage manager in New York, and for
regional and touring shows, while also dancing professionally. In 2004, she began working as a voice over artist. She subsequently left the US, and worked for the next several years as a voice over artist, while also directing musicals and performing in South Korea, Argentina, Italy, the UK, and Dubai. She returned to the US in 2011, and began directing and production managing voice over for cartoon series and pilots, commercials, promos, and educational projects, as well as writing copy for commercials and adapting scripts for animation. She opened Pepsqually VO and Sound Design in the spring of 2014.
She has since grown the company from one animated series, one engineer, and seven cast members to employing three engineers, seven writers, and 100+ actors across several projects. As of 2024, projects for which Pepsqually has provided voice over performance direction, casting, and/or sound design have received two Kidscreen Awards (among three nominations), one NAACP Image Award omination,
and eleven Parents' Choice Awards. The streaming/digital-first animated projects for which Pepsqually has provided voice over direction, casting, and/or sound design, have collectively amassed just over 1 billion views on YouTube alone.

In 2020, Paula opened a companion company in Lisbon, Portugal. Pepsqually Portugal, LDA is dedicated to the incubation and production of children's media by and for minority/marginalized populations. There are currently nine shows in the studio’s development pipeline.

In addition to casting, directing, and project managing all Pepsqually projects, Paula Gammon Wilson also serves as a freelance voice over performance director, project manager, casting director, and writer for several other production companies in the US and Europe.
