---
name: Tim Friedlander
location: USA
preview: Founder, NAVA, Voice Actor, Studio Owner
image: /speakers/img/Tim_Friedlander.jpg
order: 4
---

Tim Friedlander is a distinguished voice actor and studio owner based in Los Angeles, renowned for his talent and advocacy within the industry. With a remarkable career spanning various facets of voiceover work, Tim has solidified his position as a leading figure in the field.
As the president and co-founder of the National Association of Voice Actors (NAVA), Tim has tirelessly championed for artists’ rights, embodying a commitment to the betterment of the profession.
Tim’s voiceover portfolio boasts an impressive array of roles across animation, gaming, and television. He is known for his contributions to popular titles such as “Hunter x Hunter,” “One Punch Man,” and his portrayal of Long Caster in “Ace Combat 7.” Additionally,  performance as the English voice of Ares in the Netflix series “Record of Ragnarok” has garnered acclaim, showcasing his versatility and skill.
Throughout his career, Tim has lent his voice to diverse projects, including the Pro Bull Riders Association broadcasts on CBS and the English dub of “Beforeigners,” where he voiced the character Navn. Notably, his work on “Toymakerz,” a hit show on the Velocity Channel/History, and the History Channel series “Evil Genius” further demonstrate his range and expertise.
Tim’s talent has been recognized with prestigious awards, including the SOVAS Voice Arts Award for Best Political Voice Over in 2023, among numerous nominations across various categories. His dedication to excellence and innovation in voice acting is evident in his consistent pursuit of excellence.
Operating from his state-of-the-art studio, soundBOX:LA, Tim continues to captivate audiences with his compelling performances and unwavering commitment to his craft.
In addition to his artistic endeavors, Tim co-founded NAVA and serves as the co-owner and editor of The Voice Over Resource Guide, further cementing his role as a leader and advocate within the voiceover community.
Represented by Atlas Talent in Los Angeles and New York, as well as ACM Talent Management in LA and New York, Tim Friedlander remains a driving force in the world of voice acting, leaving an indelible mark on the industry with his talent, passion, and dedication.