---
name: Wale Ekundayo
location: Nigeria
preview: CEO/Vice President, Cerebre Media Africa, Founder, LealFC
image: /speakers/img/Wale_Ekundayo.jpg
order: 5
---

Wale is a marketing and communication CEO/Vice President, digital innovator and football enthusiast.
As a marketing expert, Wale is the founder and current Vice President of Cerebre Media Africa a marketing and communi- cation agency and SevenStaves a content and technology company in Africa. With over 25 years experiences in the industry, he has amassed vast knowledge and insight in mar- keting and communication and since Cerebre Media Africa is strategically located in three African countries, Wale has worked with leading global and continental brands looking to reach the continent of Africa.
In 2020, Wale founded LealFC a local football club. In its 3 years of incorpo- ration, LealFC has been able to host two major local club competition in partnership with both foreign and local footballing bodies. LealFC has also been able to export about $5m of Nigerian footballing skills abroad.
As a graduate of Microbiology, his passion for data analysis and research led him to start a career as the data and research analyst at, Lowe Lintas, the first advertising agency in Nigeria. Since then, Wale's passion has been driven by a data driven mar- keting and advertising approach even as he grew through the ranks across several prestigious marketing agencies in Nigeria.
Wale has worked with notable brands which include: Exxon Mobil, Microsoft Nigeria, First Bank Nigeria, Ecobank, Wema Bank, Hygeia HMO, Vilisco, House of Chi, Dabur, Pepsi, Nigeria Breweries and many more top brands. One of Wale's notable achievements was running an activation for Unilever Nigeria Lifebuoy brand, which got into the Guinness Book of World record as the largest number of children washing their hands at the same time for Global Hand washing day.
Wale also serves as the The Covenant Nation, Iganmu Campus Pastor.
