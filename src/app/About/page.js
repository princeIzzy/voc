/* eslint-disable react/no-unescaped-entities */
'use client'
import React from 'react'
import Image from 'next/image'
import { groupPic, king, audience } from '@/app/images/index'
import '@/app/styles/main.scss'
import { useEffect } from 'react'
import AOS from 'aos';
import 'aos/dist/aos.css';

const Amplify = () => {
  useEffect(() => {
    AOS.init({
      duration: 1500, 
      once: true,    
    });
  }, []);
  return (
    <main>
        <section className=' bg-white '>
          <div className='content_space2'>
            <div className='container text-center '>
                <h1 className='mobile_title text-black ft-2 title_space'data-aos="fade-in">
                  Amplifying Voices
                  <br/>
                  Across Africa and Beyond
                </h1>
              <Image src={audience} alt='audience picture' className='w-100 h-75'/>
            </div>
          </div>
        </section>

        <section  id="specificPoint" className='in2020 cu-pink' >
          <div className='content_space2'>
            <div className='container d-md-flex px-0 justify-content-between'>
              <div className='col-12 col-md-6 d-flex mb-5 mb-md-0 justify-content-center align-items-center' data-aos="fade-in" data-aos-easing="linear">
                <Image src={groupPic} alt="group picture" className='w-100 h-100 rounded-4'  />
              </div>
              <div className='col-12 col-md-5 my-auto d-flex flex-column justify-content-center p-3 overflow-y-scroll scroll_hide about-text'>
                <div className='h-100'>
                  <h1 data-aos="fade-in" data-aos-easing="linear">
                    <span className='tx-orange' >In 2020,</span>
                  </h1>
                  <p className='fw-normal ft-3 opas-1 text-black' data-aos="fade-in">
                    the Voice Over Conference was established stands as Africa's pioneer voice-over event with a singular objective: 
                    to enlighten and empower the continent's youth regarding the vast potential within voice-over acting and the expression industry.
                  </p>
                  <p className='fw-normal ft-3 opas-1 text-black' data-aos="fade-in">
                    Our overarching mission is to facilitate a process of self-discovery and development, ultimately preparing African voices for global recognition and success.
                  </p>
                  <p className='fw-normal ft-3 opas-1 text-black'>
                    The voiceover conference is brought to you by Voiceover Workshop and Media, a 360 voice over training, production, 
                    outsourcing and voiceover projects/content management production agency.
                  </p>
                  <h6 className='tx-orange'>OUR VISION WAS CLEAR:</h6>
                  <p className='fw-normal ft-3 opas-1 text-black'>
                    challenge the conventional norms and provide a nurturing platform for aspiring voice-over talents to not only succeed but flourish and find relevance and employment through the use of VOICE.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        
        <section className='dual-card'>
          <div className='content_space2'>
            <div className='container d-md-flex overflow-hidden'>
              <div className='row d-flex gap-md-4 justify-content-between'>
                <div className='col-12 col-md me-md  drop position-relative  rounded-4 p-3 p-lg-5 ' data-aos="fade-in">
                  <div className='d-flex h-100 flex-column justify-content-evenly px-md-2'>
                    <h3 className='text-white mb-5 mb-md-0 fs-2'>“Talk is cheap, but your voice can be expensive”.</h3>
                    <p className='fs-6 text-white'>King E Afemikhe -
                      <span className='fw-lighter ft opacity-75 ps-2'>Conference Convener</span>
                    </p>
                  </div>
                </div>
                <div className='col-12 col-md-7 pt-5 pt-md-0 px-md-0' data-aos="fade-in">
                  <Image src={king} alt='viatual' className='w-100 h-100 rounded-4' />
                </div>              
              </div>

            </div>
          </div>
        </section> 
        
    </main>
    
  )
}

export default Amplify
