import axios from 'axios';

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { fullName, email, phoneNumber, country, participation } = req.body;
    console.log('Registration logic executed:', { fullName, email, phoneNumber, country, participation });
    const apiKey = process.env.MAILCHIMP_API_KEY;
    const audienceId = process.env.MAILCHIMP_AUDIENCE_ID;
    const endpointUrl = `https://us4.api.mailchimp.com/3.0/lists/${audienceId}/members`;

    try {
      const headers = {
        Authorization: `apikey ${apiKey}`
      };

      const response = await axios.post(endpointUrl, {
        email_address: email,
        status: 'subscribed',
        merge_fields: {
          FNAME: fullName, 
          PHONE: phoneNumber,
          COUNTRY: country,
          ATTEND: participation
        }
      }, { headers });

      console.log('User added to Mailchimp audience:', response.data);
      res.status(200).json({ success: true });
    } catch (error) {
      console.error('Mailchimp API error:', error.response.data);
      if (error.response && error.response.data && error.response.data.title === 'Member Exists') {
        // Handle specific error case where email already exists
        return res.status(400).json({ success: false, error: 'Email already subscribed' });
      } else {
        // General error handling for other Mailchimp API errors
        res.status(500).json({ success: false, error: 'Failed to process Mailchimp request' });
      }
    }
  } else {
    res.status(405).json({ error: 'Method Not Allowed' });
  }
}