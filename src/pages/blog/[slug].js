import { useEffect } from 'react'
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import Link from 'next/link'
import Image from 'next/image'
import { emma, artist, diversity, standout } from '@/_assets/images';
import {remark}  from 'remark';
import html from 'remark-html';
import AOS from 'aos';
import 'aos/dist/aos.css';
import MorePosts from '../../../components/morePost'

export default function BlogPost({postData,otherPosts}) {
   
    useEffect(() => {
        AOS.init({
          duration: 1500, 
          once: true,    
        });
      }, []);

    return(
       <div>
          <section>
              <div className='container px-md-0 py-5 my-5'>
                  <div className='d-flex flex-column align-items-center justify-content-center ' data-aos="fade-in">
                      <h1 className='col-md-8 col-lg-6 text-center'>{postData.title}</h1>
                      {/* <div className='d-flex justify-content-center align-items-center mb-4'>
                          <Image src={postData.speaker_image} width={50} height={50} alt='bloger image' className='rounded-5'/>
                          <div className='d-flex ft'>
                            <p className='mx-3'>{postData.speaker_name}</p>
                            <p className='p-2 background-black'></p>
                            <p className=''>{postData.date}</p>
                          </div>
                      </div> */}
                  </div>
                  <div className='col-12 mb-5'data-aos="fade-in">
                      <Image width={1000} height={500} src={postData.blogcover_image} alt="blog image" className='w-100 h-100'  />
                  </div>
                  <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className='mx-lg-5' data-aos="fade-in"/>
              </div>
          </section>
          <MorePosts otherPosts={otherPosts} />
       </div> 
    )
}

export async function getStaticPaths() {
    const files = fs.readdirSync(path.join('posts'))

    const paths = files.map(filename => ({
        params: {
            slug: filename.replace('.md', '')
        }
    }))

    return{
        paths,
        fallback: false
    }
}

export async function getStaticProps({ params }) {
    const { slug } = params;
    const fullPath = path.join('posts', slug + '.md');
    
    // Read file asynchronously
    const fileContents = fs.readFileSync(fullPath, 'utf8');
    
    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents);

    // Use remark to convert markdown into HTML string
    const processedContent = await remark()
        .use(html)
        .process(matterResult.content);
    const contentHtml = processedContent.toString();
    
    // Combine the data with the id and contentHtml
    const postData = {
        slug,
        contentHtml,
        ...matterResult.data,
    };

     // Get other post slugs excluding the current post
    const files = fs.readdirSync(path.join('posts'));
    const otherPostSlugs = files
        .map((filename) => filename.replace('.md', ''))
        .filter((otherSlug) => otherSlug !== slug);
        
    // Fetch data for other posts
    const otherPosts = otherPostSlugs.map((otherSlug) => {
        const otherFullPath = path.join('posts', otherSlug + '.md');
        const otherFileContents = fs.readFileSync(otherFullPath, 'utf8');
        const otherMatterResult = matter(otherFileContents);

        return {
            slug: otherSlug,
            title: otherMatterResult.data.title,
            subtitle: otherMatterResult.data.subtitle,
            cover_image: otherMatterResult.data.cover_image,
            speaker_image: otherMatterResult.data.speaker_image,
            speaker_name: otherMatterResult.data.speaker_name,
        };
    });

    return {
        props: {
            postData,
            otherPosts,
        },
    };
}