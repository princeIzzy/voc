import { useEffect } from 'react'
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import Link from 'next/link'
import PostPreview from '../../../components/PostPreview'
import Image from 'next/image';
import AOS from 'aos';
import 'aos/dist/aos.css';

export default function TestBlog ({posts}) {
  useEffect(() => {
    AOS.init({
      duration: 1500, 
      once: true,    
    });
  }, []);

  const firstPost = posts.find(post => post.frontmatter.title === '4 Ways to Learn Accent as A Voiceover Artist');

  return (
    <div>
      
      <section>
        {firstPost && 
          <div className='container px-md-0 py-5 my-5'>
            <Link  href={`/blog/${firstPost.slug}`} passHref className='text-decoration-none text-black'>
              <div className='border border-1 shadow d-lg-flex'>
                <div className='col-12 col-lg-7 d-flex  justify-content-center align-items-center rounded-start' data-aos="fade-in">
                  <Image height={477} width={700} src={firstPost.frontmatter.cover_image} alt='cover image' className='w-100 h-100'/>
                </div>
                <div className='col-12 col-lg-5 my-4 pb-4 d-flex flex-column justify-content-md-between px-3 px-md-5' data-aos="fade-in">
                    <div className=''>
                        <h1>{firstPost.frontmatter.title}</h1>
                        <p className='fs-6 '>{firstPost.frontmatter.subtitle}</p>
                        <div className='d-flex justify-content-start'>
                            <div className='  p-2 ft  lightOrage'>{firstPost.frontmatter.date}</div>                        
                        </div>
                    </div>
                    <div className='d-flex align-items-center mt-5 mt-xl-0 '>
                      {/* <Image src={firstPost.frontmatter.speaker_image} width={50} height={50} alt='' className='rounded-5'/>
                      <p className='mb-0 ms-3'>{firstPost.frontmatter.speaker_name}</p> */}
                    </div>
                </div>
              </div>
            </Link>
          </div> 
        }
        
      </section>
    
      {/* Map through the rest of the posts excluding the first post */}
      <section className='container px-md-0'>
        <div className='row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center g-4 title_space '>
        {posts
          .filter(post => post.frontmatter.title !== '4 Ways to Learn Accent as A Voiceover Artist')
          .map((post, index) => (
            <Link key={index} href={`/blog/${post.slug}`} passHref className='text-decoration-none text-black'>
                <div className='col' data-aos="fade-in">
                  <PostPreview post={post} />
                </div>
            </Link>
        ))}
        </div>
      </section>
    
    </div>
  )
}

export async function getStaticProps() {
  //Get files from the posts directory
  const files = fs.readdirSync(path.join('posts'))

  //get slug and frontmatter from posts
  const posts = files.map((filename) => {
    //Create slug
    const slug = filename.replace('.md','')

    //Get frontmatter
    const markdownWithMeta = fs.readFileSync(path.join('posts',filename), 'utf-8')
    const {data:frontmatter} = matter(markdownWithMeta)
    return {
        slug,
        frontmatter,
    }
  });
  // console.log(posts)
  return {
    props: {
      posts,
    },
  };
}