'use client'
import {useState, useEffect } from 'react'
import Image from 'next/image'
import { nigeria } from '../../_assets/images'
import AOS from 'aos';
import 'aos/dist/aos.css';
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/main.css';

const Contact = () => {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
    description: '',
  });

  const [successMessage, setSuccessMessage] = useState('');
  
  useEffect(() => {
    AOS.init({
      duration: 1500, 
      once: true,    
    });
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
  
    const response = await fetch('/api/email', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });
  
    if (response.ok) {
      // Reset the form and display success message
      setFormData({
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        description: '',
      });
  
      setSuccessMessage('Details sent successfully');
    } else {
      // Handle error, e.g., show an error message
      console.error('Error sending details');
      setSuccessMessage('Details not sent');
    }
    setTimeout(() => {
      setSuccessMessage('');
    }, 5000);
  };

  return (
    <div>
      <section className='contact-hero content_space2'>
        <div className='container px-md-0 d-block d-lg-flex overflow-hidden'>
          <div className='col col-lg-5 d-flex flex-column justify-content-center overflow-hidden'>
            <h1 className='clash ft-4 pe-5 me-4 text-black mobile_title' data-aos="fade-in">
              We’d love to hear from you
            </h1>
            <p className='ft-3'data-aos="fade-in">
              Fill this form and we would get back to you
            </p>
          </div>
          <div className='col col-lg-7 bg-black p-3 rounded cover' >
            <div className=''data-aos="fade-in">
              <h2 className='text-white clash fs-2'>Send us a message</h2>
              <form onSubmit={handleSubmit} className='p-2 '>
                <div className='mb-3 d-block d-md-flex gap-3'>
                  <input type="text" name='firstName' value={formData.firstName} onChange={(e) => setFormData({ ...formData, firstName: e.target.value })} required minLength={3} placeholder='First name' className='contact-form border-0 w-100 w-md-50 rounded mb-3 mb-md-0  p-3 footer-input text-white ft'></input>  
                  <input type="text" name='lastName' value={formData.lastName} onChange={(e) => setFormData({ ...formData, lastName: e.target.value })}required minLength={3} placeholder='Last name' className='contact-form border-0 w-100 w-md-50 rounded p-3 footer-input text-white ft'></input>  
                </div>
                <div className='mb-3 d-block d-md-flex gap-3'>
                  <input type="email" name='email' value={formData.email} onChange={(e) => setFormData({ ...formData, email: e.target.value })} required minLength={3} placeholder='Email address' className='contact-form border-0 w-100 w-md-50 rounded mb-3 mb-md-0 p-3 footer-input text-white ft'></input>  
                  <div className='w-100 w-md-50 contact-form border-0 rounded ft'>
                    <IntlTelInput
                      inputClassName='intl-tel-input '
                      preferredCountries={['NG']}
                      autoHideDialCode={true}
                      separateDialCode={true}
                      formatOnInit={true}
                      placeholder='Phone number'
                      format={(selectedCountry) => {
                        return `+${selectedCountry.dialCode} (${selectedCountry.iso2})`; // Format to include country code
                      }}
                      onPhoneNumberChange={(isValid, phoneNumber, countryData) => {
                        setFormData({ ...formData, phoneNumber: phoneNumber });
                      }}
                    />
                  </div>
                </div>
                <textarea type="text" name='description' value={formData.description} onChange={(e) => setFormData({ ...formData, description: e.target.value })} required minLength={3} placeholder='How can we help?' className='contact-form mb-3 w-100 rounded text-start border-0 p-2 pt-0  h-6 text-white ft'></textarea> 
                <button
                  type="submit"
                  className={`w-100 border-0 py-3 text-white ft rounded 
                  ${successMessage.includes('Details sent successfully')? 'bg-success text-white' : successMessage.includes('Details not sent')? 'bg-danger text-white' : 'orange'}`}
                >
                  {successMessage === 'Details sent successfully' ? 'Successful' : successMessage === 'Details not sent' ? 'Unsuccessful' : 'Send message'}
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section className='content_space2'>
        <div className='container px-md-0'>
          <div className='d-lg-flex  '>
            <div className='gap-4 px-auto px-md-5 col-12 d-lg-flex'>
              <div className='col-12 col-lg rounded mb-4 mb-lg-0 w-md-50 mx-auto shadow px-3 py-4' data-aos="fade-in">
                <h3 className='clash_light mb-3 fw-semibold fs-5'>Contact support team</h3>
                <div className='d-md-flex justify-content-between'>
                  <div>
                    <p className='mb-0 ft'>
                      Email<br/>
                      <span className='cu-gray1 contact-email '>support@thevoiceoverconference.com</span>
                    </p>
                  </div>
                  <div>
                    <p className='mb-0 ft'>
                      Contact<br/>
                      <span className='cu-gray1 contact-email '>+2348088809661</span>
                    </p>
                  </div>
                </div>
              </div>
              <div className='col-12 col-lg rounded mb-4 mb-md-0 w-md-50 mx-auto shadow px-3 py-4'data-aos="fade-in">
                <h3 className='clash_light mb-3 fw-semibold fs-5'>Contact admin</h3>
                <div className='d-md-flex justify-content-between'>
                  <div>
                    <p className='mb-0 ft'>
                      Email<br/>
                      <span className='cu-gray1'>info@thevoiceoverconference.com</span>
                    </p>
                  </div>
                  <div>
                    <p className='mb-0'>
                      Contact<br/>
                      <span className='cu-gray1'>+2348088809661</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default Contact
