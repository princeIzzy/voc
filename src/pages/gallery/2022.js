'use client'
import React from 'react'
import Image from 'next/image'
import { gallary13, gallary14, gallary15, gallary16, gallary17, gallary18, gallary19, gallary20,gallary21,gallary22, gallary23, gallary24,} from '../../_assets/images'
import { useEffect } from 'react'
import AOS from 'aos';
import 'aos/dist/aos.css';

const Gallery = () => {
  useEffect(() => {
    AOS.init({
      duration: 1500, 
      once: true,    
    });
  }, []);

  return (
    <div>
      <section className='  my-auto'>
        <div className='speaker-hero d-flex align-items-center justify-content-center'>
          <h1 className='mobile_title text-white z-1 w-75 w-md-50 fw-bold text-center ' data-aos="fade-in">
            Exciting Moments TVOC2022
          </h1>
        </div>
      </section>
      <section className='content_space2  bg-white'>
        <div className='container px-md-0'>
          <div className='grid_cont mb-3'>
              <div className='grid_item1 grid_fit  ' data-aos="fade-in">
                <Image src={gallary19} alt='speaker' className='w-100 h-100 rounded-4'/>
              
              </div>
              <div className='grid_item2 grid_fit ' data-aos="fade-in">
                <Image src={gallary18} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item3 grid_fit  ' data-aos="fade-in">
                <Image src={gallary14} alt='speaker' className='w-100 h-100 rounded-4'/>
              
              </div>
              <div className='grid_item4 grid_fit' data-aos="fade-in">
                <Image src={gallary15} alt='speaker' className='w-100 h-100 rounded-4'/>
              
              </div>
              <div className='grid_item5 grid_fit' data-aos="fade-in">
                <Image src={gallary13} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item6 grid_fit  ' data-aos="fade-in">
                <Image src={gallary16} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
          </div>
          <div className='grid_cont2 '>
              <div className='grid_item7 grid_fit ' data-aos="fade-in">
                <Image src={gallary23} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item8 grid_fit  ' data-aos="fade-in">
                <Image src={gallary21} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item9 grid_fit ' data-aos="fade-in">
                <Image src={gallary17} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item10 grid_fit' data-aos="fade-in">
                <Image src={gallary22} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item11 grid_fit  ' data-aos="fade-in">
                <Image src={gallary24} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item12 grid_fit  ' data-aos="fade-in">
                <Image src={gallary20} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
          </div>
      </div>
      </section>
    </div>
  )
}

export default Gallery
