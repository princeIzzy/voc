'use client'
import React from 'react'
import Image from 'next/image'
import { gallary25, gallary28, gallary27, gallary31, gallary26, gallary29, gallary30, gallary32, gallary33, gallary34,gallary35, gallary36 } from '../../_assets/images'
import { useEffect } from 'react'
import AOS from 'aos';
import 'aos/dist/aos.css';

const Gallery = () => {
  useEffect(() => {
    AOS.init({
      duration: 1500, 
      once: true,    
    });
  }, []);

  return (
    <div>
      <section className='  my-auto'>
        <div className='speaker-hero d-flex align-items-center justify-content-center'>
          <h1 className='mobile_title text-white z-1 w-75 w-md-50 fw-bold text-center ' data-aos="fade-in">
            Exciting Moments TVOC2024
          </h1>
        </div>
      </section>
      <section className='content_space2  bg-white'>
        <div className='container px-md-0'>
          <div className='grid_cont mb-3'>
              <div className='grid_item1 grid_fit  ' data-aos="fade-in">
                <Image src={gallary25} alt='speaker' className='w-100 h-100 rounded-4'/>
              
              </div>
              <div className='grid_item2 grid_fit ' data-aos="fade-in">
                <Image src={gallary34} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item3 grid_fit  ' data-aos="fade-in">
                <Image src={gallary27} alt='speaker' className='w-100 h-100 rounded-4'/>
              
              </div>
              <div className='grid_item4 grid_fit' data-aos="fade-in">
                <Image src={gallary28} alt='speaker' className='w-100 h-100 rounded-4'/>
              
              </div>
              <div className='grid_item5 grid_fit' data-aos="fade-in">
                <Image src={gallary29} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item6 grid_fit  ' data-aos="fade-in">
                <Image src={gallary30} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
          </div>
          <div className='grid_cont2 '>
              <div className='grid_item7 grid_fit ' data-aos="fade-in">
                <Image src={gallary31} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item8 grid_fit  ' data-aos="fade-in">
                <Image src={gallary32} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item9 grid_fit ' data-aos="fade-in">
                <Image src={gallary33} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item10 grid_fit' data-aos="fade-in">
                <Image src={gallary26} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item11 grid_fit  ' data-aos="fade-in">
                <Image src={gallary36} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
              <div className='grid_item12 grid_fit  ' data-aos="fade-in">
                <Image src={gallary35} alt='speaker' className='w-100 h-100 rounded-4'/>
                
              </div>
          </div>
      </div>
      </section>
    </div>
  )
}

export default Gallery