'use client'
import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';
import Image from 'next/image'
import Link from 'next/link';
import { useRouter } from 'next/router'
import Clock from '../../src/_assets/animations/animation1.json';
import Wave from '../../src/_assets/animations/animation2.json';
import {mic_guy,mic_guy2,mic_guy3,mic_lady1,mic_lady2,mic_lady3,gradient1,gradient2, conference,
africa, card_img1,card_img2,card_img3,CalledOut,Nava,waystream,shutter,VoiceVerse,fusions,pundit,Finserve,
Rapid,
nashen,
lion} 
from '../_assets/images';
import { useEffect } from 'react'
import AOS from 'aos';
import 'aos/dist/aos.css';
import Lottie from 'lottie-react';
import Countdown from '../../components/Countdown'
import SpeakerInfo from '../../components/SpeakerInfo'

export default function Home({speakers}) {

  useEffect(() => {
    AOS.init({
      duration: 1500, 
      once: true,    
    });
  }, []);

  const style = {
    height: 45,
    width:80
  };

  // const WaveGif = {
  //   loop: true,
  //   autoplay: true,
  //   animationData: Wave,
  //   rendererSettings: {
  //     preserveAspectRatio: 'xMidYMid slice',
  //   },
  // };
  // const clockGif = {
  //   loop: true,
  //   autoplay: true,
  //   animationData: Clock,
  //   rendererSettings: {
  //     preserveAspectRatio: 'xMidYMid slice',
  //   },
  // };

  const targetDate = new Date('2025-06-26T00:00:00');
  const router = useRouter()
  return (
    <main className='h-full'>
      <section className='hero container overflow-hidden  bg-white d-lg-flex '>
        <div className='col-12 col-lg-6 pe-md-5 my-5 my-lg-auto'>
          <h1 className='mobile_title fw-bold ft-4 ' data-aos="fade-in">Pioneer And Biggest Voiceover Event in Africa</h1>
          <p className='text-black opacity-75 mb-5 fs-5 fw-bold' data-aos="fade-in"> Learn. Connect. Grow</p>
          <div className=''>
            <button onClick={() => router.push('/register')} className='register_btn border-0 rounded orange py-3 px-4 text-white ft me-3'>Register</button>
            <button onClick={() => router.push('https://paystack.com/pay/j62ehjlti1')} className='sponsor_btn rounded  py-3 px-4 text-black bg-white ft '>Sponsor</button>
          </div>
        </div>
        <div className='col-12 col-lg-6 overflow-hidden'>
          <div className='hero_cont position-relative d-flex '>
            <div className='position-absolute-md track_cont d-block w-50 '>
              <div className='hero_track1 d-flex flex-column justify-content-between '>
                <div className='mb-3 me-2'>
                  <Image src={mic_lady1} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 me-2 position-relative overflow-hidden'>
                  <Image src={gradient1} alt='gradient' className='w-100 h-100 rounded position-relative' />
                  <div className='position-absolute wave_style w-50 h-25'>
                    <Lottie animationData={Wave}  />
                  </div>
                </div>
                <div className='mb-3 me-2'>
                  <Image src={mic_guy2} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 me-2'>
                  <Image src={mic_lady3} alt='presenter' className='w-100 h-100 rounded' />
                </div>
              </div>
              {/* repeat */}
              <div className='hero_track1 d-flex flex-column justify-content-between '>
                <div className='mb-3 me-2'>
                  <Image src={mic_lady1} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 me-2 position-relative overflow-hidden'>
                  <Image src={gradient1} alt='gradient' className='w-100 h-100 rounded position-relative' />
                  <div className='position-absolute wave_style w-50 h-25'>
                    <Lottie animationData={Wave}  />
                  </div>
                </div>
                <div className='mb-3 me-2'>
                  <Image src={mic_guy2} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 me-2'>
                  <Image src={mic_lady3} alt='presenter' className='w-100 h-100 rounded' />
                </div>
              </div>
            </div>
            <div className='position-absolute track_cont end-0 d-block w-50'>
              <div className='hero_track2 d-flex flex-column justify-content-between'>
                <div className='mb-3 ms-2'>
                  <Image src={mic_guy} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 ms-2'>
                  <Image src={mic_lady2} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 ms-2 position-relative overflow-hidden'>
                  <Image src={gradient2} alt='gradient' className='w-100 h-100 rounded' />
                  <div className='position-absolute wave_style w-50 h-25'>
                    <Lottie animationData={Wave} />
                  </div>
                </div>
                <div className='mb-3 ms-2'>
                  <Image src={mic_guy3} alt='presenter' className='w-100 h-100 rounded' />
                </div>
              </div>
              {/* repeat */}
              <div className='hero_track2 d-flex flex-column justify-content-between'>
                <div className='mb-3 ms-2'>
                  <Image src={mic_guy} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 ms-2'>
                  <Image src={mic_lady2} alt='presenter' className='w-100 h-100 rounded' />
                </div>
                <div className='mb-3 ms-2 position-relative overflow-hidden'>
                  <Image src={gradient2} alt='gradient' className='w-100 h-100 rounded' />
                  <div className='position-absolute wave_style w-50 h-25'>
                    <Lottie animationData={Wave} />
                  </div>
                </div>
                <div className='mb-3 ms-2'>
                  <Image src={mic_guy3} alt='presenter' className='w-100 h-100 rounded' />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className='slider-cont container my-5 my-sm-0 px-0 '  >
        <div className='row overflow-hidden d-none'>
          <div  className='logo_slider  position-relative' >
            <div className='logo_sliderTrack d-flex justify-content-center overflow-hidden' data-aos="fade-in">
              <div className=' d-flex w-100 align-items-center justify-content-between'>
                <div className="logo_card">
                  <Image src={lion} className="w-100 h-100" alt="..." />
                </div>            
                <div className="logo_card">
                  <Image src={shutter} className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={VoiceVerse} className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={Nava} className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={fusions} className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={pundit} className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={CalledOut} className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={waystream} className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={Finserve} className="w-100 h-100" alt="..." />
                </div>            
                <div className="logo_card">
                  <Image src={Rapid} className="w-100 h-100" alt="..." />
                </div>            
                <div className="logo_card">
                  <Image src={nashen} className="w-100 h-100" alt="..." />
                </div>            
              </div>

              {/* repeat */}
              <div className='d-lg-none d-flex align-items-center justify-content-between'>
                <div className="logo_card">
                  <Image src={lion} className="w-100 h-100" alt="..." />
                </div>          
                <div className="logo_card">
                  <Image src={shutter}  className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={VoiceVerse}  className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={Nava}  className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={fusions}  className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={pundit}  className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={CalledOut}  className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={waystream}  className="w-100 h-100" alt="..." />
                </div>
                <div className="logo_card">
                  <Image src={Finserve}  className="w-100 h-100" alt="..." />
                </div>    
                <div className="logo_card">
                  <Image src={Rapid} className="w-100 h-100" alt="..." />
                </div>            
                <div className="logo_card">
                  <Image src={nashen} className="w-100 h-100" alt="..." />
                </div>          
              </div>
            </div>
          </div>
        </div>
      </section> 

      <section className='timer container px-0 d-none d-md-flex sticky-top w-100 justify-content-center align-items-center'>
        <div className=' px-2 py-3 rounded-5 w-100 position-relative  b-drop'>
          <div className='d-lg-flex  justify-content-between px-4'>
            <div className='d-flex col-12 col-lg-7 mx-auto align-items-center'>
              <div><Lottie animationData={Clock} style={style} /></div>
              <h5 className='text-white my-auto'>Get ready for The Voiceover Conference 5.0</h5>
            </div>
            <Countdown targetDate={targetDate} />
          </div>
        </div>
      </section>

      <section className='empowering lightBlue ' >
        <div className='content_space'>
          <div className=' container d-lg-flex ' >
            <div className='col-12 col-lg-6 d-flex justify-content-center align-items-center mb-5 mb-lg-0'  data-aos="fade-in">
              <Image src={africa} alt="africa's map" className='w-100 h-100 px-md-5'  />
            </div>
            <div className='col-12 col-lg-6 d-flex flex-column justify-content-center px-md-5'>
              <h1 className='d-flex justify-content-center justify-content-lg-start' data-aos="fade-in">Empowering <br/> Africa&apos;s Voiceover Talents</h1>
              <p className='fw-normal fs-6 lh-2 cu-black1 ' data-aos="fade-in">
                The Voice Over Conference is Africa&apos;s pioneer voice-over conference founded to change the status quo in the voiceover industry in Africa. 
              </p>
              <p className='fw-normal fs-6 lh-2 cu-black1 ' data-aos="fade-in">
                TVOC seeks to transform voiceover into a thriving sector that not only generates profitable business opportunities to young Africans but also
                fosters employment and facilitates meaningful networking among the emerging African Voiceover artists on the continent.
              </p>
              <p className='fw-normal fs-6 lh-2 cu-black1 ' data-aos="fade-in">
                With the rise of unemployment in Africa, the voiceover conference seeks to harness a green industry like voiceover for profitable business, employment, 
                and relevant network opportunities among young creative minds, entrepreneurs and business owners…… .
              </p>
              <Link href='/about#specificPoint' className=''>
                <button  className='register_btn border-0 rounded orange py-3 px-4 text-white ft'>Learn More</button>
              </Link>
            </div>
          </div>
        </div>
      </section>

      <section className='card_sec cu-pink '>
        <div className='content_space'>
          <div className=' container overflow-hidden' >
            <div className='text-center title_space' data-aos="fade-in" >
              <h1>A 2 Day Hybrid Event <br/> What To Expect</h1>
            </div>
            <div className='row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center g-4'>
              <div className='col' data-aos="fade-in">
                <div className="card rounded">
                  <Image src={card_img1} className="card-img" alt="..." height={430}/>
                  <div className="card-img-overlay p-md-5 lin d-flex flex-column justify-content-end">
                    <div className='card_content'>
                      <h3 className="card-title text-white ">LEARN</h3>
                      <p className="card-text card_effect text-white ft">
                        Participants in the voiceover conference stand to learn and benefit from skill development, 
                        industry insights, networking and collaboration, business and entrepreneurship knowledge, 
                        and global exposure, all of which can contribute to their growth and success in the voiceover industry.
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col' data-aos="fade-in">
                <div className="card rounded">
                  <Image src={card_img2} className="card-img" alt="..." height={430} />
                  <div className="card-img-overlay p-md-5 lin d-flex flex-column justify-content-end">
                    <div className="card_content">
                      <h3 className="card-title text-white">GROW</h3>
                      <p className="card-text card_effect text-white ft">
                        The African Pioneer Voiceover conference is aimed at bringing about growth among Voiceover talents and practitioners by providing exposure to learning, 
                        enhancing skills, creating employment opportunities, offering global exposure, and fostering networking and collaboration within the industry.
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col' data-aos="fade-in">
                <div className="card rounded">
                  <Image src={card_img3} className="card-img" alt="..." height={430} />
                  <div className="card-img-overlay p-md-5 lin d-flex flex-column justify-content-end">
                    <div className="card_content">
                      <h3 className="card-title text-white">CONNECT</h3>
                      <p className="card-text card_effect text-white ft">
                        Creating a platform for the largest singular African pioneer voiceover conference for all voice over actors and practitioners to learn, 
                        share and network amongst each other
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className='dual-card'>
        <div className='content_space '>
          <div className='container px-0 d-md-flex'>
            <div className='row d-flex overflow-hidden'>
              <div className='col-12 col-lg-7  h-auto dual-card-img ' data-aos="fade-in">
                <Image src={conference} alt='viatual' className='w-100 h-100 rounded-4' />
              </div>
              <div className='col-12 col-lg-5 drop position-relative rounded-4 h-auto p-md-5 py-4 mt-4 mt-md-0 d-flex flex-column justify-content-between' data-aos="fade-in">
                <h2 className='text-white title_space'>Impactful  Conference Experience</h2>
                <ul className='list-unstyled text-white'>
                  <li className='d-flex align-items-center mb-4'>
                    <div className='me-3'>
                      <span className='fw-bold px-3 w-100 text-nowrap py-3 rounded-3 ft-1 d-flex align-items-center orange-1'>DATE</span>
                    </div>
                    <p className='fw-bold ft-1 mb-0'>
                      Thursday JUNE 26th & Friday JUNE 27th 2025
                    </p>
                  </li>
                  <li className='d-flex align-items-center mb-4'>
                    <div className='me-3'>
                      <span className='fw-bold px-3 w-100 text-nowrap py-3 rounded-3 ft-1 d-flex align-items-center orange-1 '>DAY 1</span>
                    </div>
                    <p className='fw-bold ft-1 mb-0'>
                      12 NOON ~ Virtual Sessions Only
                    </p>
                  </li>
                  <li className='d-flex align-items-center'>
                    <div className='me-3'>
                      <span className='fw-bold px-3 w-100 text-nowrap py-3 rounded-3 ft-1 d-flex align-items-center orange-1 '>DAY 2</span>
                    </div>
                    <p className='fw-bold ft-1 mb-0'>
                      {/* 09:00 AM ~ Hybrid Sessions <br/> (Virtual and In-person Sessions) */}
                      {/*  */}
                      Red Carpet ~ 9:00 AM.<br/> Doors Open ~ 10:30AM
                      <br/>
                      In-person Sessions - The Covenant Place, Iganmu. Beside National Arts Theatre, Lagos, Nigeria

                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section> 

      <section className='multi-slide lightBlue'>
        <div className='content_space'>
          <div className='title_space'>
            <h1 className='text-black container d-flex justify-content-center justify-content-lg-start' data-aos="fade-in">Featured Speakers</h1>
          </div>
          <div className='container-fluid justify-content-center px-0'>
            <h1 className='d-flex justify-content-center'>Coming soon !</h1>
            {/* First slider */}
            {/* <div className='slider overflow-hidden position-relative'>
              <div className='sliderTrack position-absolute d-flex'>
                {speakers.slice(0, 14).map((speaker, index) => (
                  <div key={index} className='w-md-50 d-flex justify-content-between'>
                    <SpeakerInfo
                      imageSrc={speaker.image}
                      name={speaker.name}
                      role={speaker.preview}
                    />
                  </div>
                ))}
                {speakers.slice(0, 14).map((speaker, index) => (
                  <div key={index} className='w-md-50 d-flex justify-content-between'>
                    <SpeakerInfo
                      imageSrc={speaker.image}
                      name={speaker.name}
                      role={speaker.preview}
                    />
                  </div>
                ))}
              </div>
            </div> */}
            {/* Second slider */}
            {/* <div className='slider overflow-hidden position-relative'>
              <div className='sliderTrackx position-absolute d-flex'>
                {speakers.slice(15, 28).map((speaker, index) => (
                  <div key={index} className='w-md-50 d-flex justify-content-around'>
                    <SpeakerInfo
                      imageSrc={speaker.image}
                      name={speaker.name}
                      role={speaker.preview}
                    />
                  </div>
                ))}
                {speakers.slice(15, 28).map((speaker, index) => (
                  <div key={index} className='w-md-50 d-flex justify-content-around'>
                    <SpeakerInfo
                      imageSrc={speaker.image}
                      name={speaker.name}
                      role={speaker.preview}
                    />
                  </div>
                ))}
              </div>
            </div> */}
            {/* Third slider */}
            {/* <div className='slider overflow-hidden position-relative'>
              <div className='sliderTrack position-absolute d-flex'>
                {speakers.slice(29, 42).map((speaker, index) => (
                  <div key={index} className='w-md-50 d-flex justify-content-between'>
                    <SpeakerInfo
                      imageSrc={speaker.image}
                      name={speaker.name}
                      role={speaker.preview}
                    />
                  </div>
                ))}
                {speakers.slice(29, 42).map((speaker, index) => (
                  <div key={index} className='w-md-50 d-flex justify-content-between'>
                    <SpeakerInfo
                      imageSrc={speaker.image}
                      name={speaker.name}
                      role={speaker.preview}
                    />
                  </div>
                ))}
              </div>
            </div> */}
          </div>
        </div>
      </section>

      <section className='booking cu-pink'>
        <div className='content_space'>
          <div className=' container d-flex flex-column justify-content-center align-items-center'>
            <h1 className='title_space'  data-aos="fade-in" >Get Ticket</h1>
            <div className=' d-block d-md-flex w-100 gap-4 justify-content-center'>
              <div className='col-12 col-md col-lg-4 mb-4 mb-md-0 bg-white p-5 rounded' data-aos="fade-in">
                <div className='d-flex justify-content-center mb-3'>
                  <h5 className='text-nowrap'>Attend for Free</h5>
                </div>
                <div>
                  <button onClick={() => router.push('/register')} className='register_btn orange p-3 w-100 border-0 rounded-1 ft text-white'>Click here to register</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}
export async function getStaticProps() {
  const speakersDirectory = path.join(process.cwd(), 'public', 'speakers');
  const files = fs.readdirSync(speakersDirectory);

  const speakers = files
    .filter(file => file.endsWith('.md'))
    .map(file => {
      const filePath = path.join(speakersDirectory, file);
      const fileContents = fs.readFileSync(filePath, 'utf8');
      const { data: frontmatter } = matter(fileContents);
      return {
        ...frontmatter,
        image: frontmatter.image, // Ensure image paths are correct
      };
    });
  const orderedSpeakers = speakers.sort((a, b) => a.order - b.order);

  return {
    props: {
      speakers: orderedSpeakers,
    },
  };
}