'use client'
import { newlogo1,reg} from '@/_assets/images';
import { useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import Select from 'react-select';
import { countries } from 'countries-list';


const allCountries = Object.entries(countries).map(([countryCode, country]) => ({
  value: countryCode,
  label: `${country.name} (+${country.phone})`,
}));
const phoneCodes = Object.entries(countries).map(([countryCode, country]) => ({
  value: `+${country.phone}`,
  label: `+${country.phone}`,
}));

const participationOptions = [
  // {
  //   value: 'Physical attendance only',
  //   label: ' Physical attendance only'
  // },
  {
    value: 'Virtual  attendance only',
    label: 'Virtual  attendance only'
  },
  // {
  //   value: 'Hybrid attendance',
  //   label: 'Hybrid attendance'
  // },
]


const Register = () => {  
  const [formData, setFormData] = useState({
    fullName: '',
    email: '',
    phoneNumber: '',
    country: '',
    participation: '',
  });

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [buttonColour, setButtonColour] = useState('orange');
  const [buttonText, setButtonText] = useState('Register');

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };
  
  const handleSelectChange = (selectedOption, { name }) => {
    setFormData((prevData) => ({ ...prevData, [name]: selectedOption.value }));
  };
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsSubmitting(true);
  try{
     const fullPhoneNumber = formData.country + formData.phoneNumber;
    const updatedFormData = { ...formData, fullPhoneNumber };
    const response = await fetch('/api/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updatedFormData),
    });
    
    if (response.ok) {
      setButtonColour('bg-success');
      setButtonText('Congratulations! You have successfully registered');
      setFormData({
        fullName: '',
        email: '',
        phoneNumber: '',
        country: '',
        participation: '',
      });
    } else {
      const responseData = await response.json();
      if (response.status === 400 && responseData.error === 'Email already subscribed') {
        setButtonColour('orange');
        setButtonText('You have already registered. Thank you.');
      } else {
        setButtonColour('bg-danger');
        setButtonText('Unable to Register. Please, try again or contact administrator');
      }
    }
  } catch (error){
    setButtonColour('bg-danger');
    setButtonText('Unable to Register. Please, try again or contact administrator');
  }
   
    setIsSubmitting(false);
    // Reset registration status after 5 seconds
    setTimeout(() => {
      setButtonColour('orange');
      setButtonText('Register');
    }, 5000);
  };
    
      
  return (
    <div>
      <section>
        <div className='reg-cont px-md-0 d-block d-lg-flex '>
          <div className='col bg-white p-3 rounded cover' >
            <div className=''data-aos="fade-in">
              <div className='col-5 title_space'>
                <Link href="/">
                  <Image src={newlogo1} alt='Logo'  className='w-auto'/>
                </Link>
              </div>
              <h2 className='mb-4 text-black clash fs-2'>Registration details</h2>
              <form onSubmit={handleSubmit} className='p-2 '>
                <div className='mb-3 gap-3'>
                  <label htmlFor="" className="form-label fw-bold raleway-reg">Full name</label>
                  <input type="text" name="fullName" value={formData.fullName} onChange={handleChange} required placeholder='Enter your full name' className=' reg fw-normal raleway-reg w-100 w-md-50 rounded mb-3 mb-md-0  p-3 text-black ft'></input>  
                </div>
                <div className='mb-3 gap-3'>
                  <label htmlFor="" className="form-label fw-bold raleway-reg">Email address</label>
                  <input type="email" name="email" value={formData.email} onChange={handleChange} required  placeholder='Enter your email address' className=' reg fw-normal raleway-reg w-100 w-md-50 rounded mb-3 mb-md-0 p-3 text-black ft'></input>  
                </div>
                <div className='mb-3 gap-3'>
                  <label htmlFor="phoneNumber" className="form-label fw-bold raleway-reg">Phone numbers</label>
                  <div className='d-flex justify-content-between'>
                    <Select options={phoneCodes} classNamePrefix="custom-select" onChange={(selectedOption) => handleSelectChange(selectedOption, { name: 'countryCode' })} placeholder="+234" /> 
                    <input type="tel" name="phoneNumber" value={formData.phoneNumber} onChange={handleChange} required placeholder='Enter your phone number' className='ms-2 reg raleway-reg col rounded mb-3 mb-md-0 p-3 text-black ft'></input>  
                  </div>
                </div>
                <div className='mb-3 gap-3'>
                  <label htmlFor="country" className="form-label fw-bold raleway-reg raleway-reg">Country</label>
                  <Select options={allCountries} onChange={(selectedOption) => handleSelectChange(selectedOption, { name: 'country' })}  classNamePrefix="custom-select" placeholder="Select country" />
                </div>
                <div className='mb-3 gap-3'>
                  <label htmlFor="participation" className="form-label fw-bold raleway-reg raleway-reg">How will you be participating</label>
                  <Select options={participationOptions} onChange={(selectedOption) => handleSelectChange(selectedOption, { name: 'participation' })} classNamePrefix='custom-select' required placeholder='Select'/>
                </div>
                
                <button
                  type="submit"
                  className={`w-100 border-0 py-3 text-white ft rounded ${buttonColour}`}
                  disabled={isSubmitting}
                >
                  {isSubmitting ? 'Sending...' : buttonText}
                </button>
              </form>
            </div>
          </div>
          <div className='d-none d-lg-block col-7'>
            <Image src={reg} alt='registration image' className='w-100 h-100'/>
          </div>
        </div>
      </section>
    </div>
  )
}
Register.getLayout = (page) => <>{page}</>;
export default Register