import React from 'react'
import Image from 'next/image'
import { Mic,king, Joshua, king2, tolulope, leslie, segunX, Edewor, koromone, Femi, Ferdinand, OjSkillz} from '../../_assets/images'
import Commercial from '../../../components/Commercials'
import Panelist from '../../../components/Panelist'
import { CiLocationOn } from 'react-icons/ci'
import Host from '../../../components/Host'
import { Ayomide, Charlene, Charles, Chilu, David, Eniola, Esther, ajayi, Kamiza,oluwaseun, Kwesi, wale, serah, Lau, Marc, Nadeem, Nicholas, Paula, Shobo, Tim, Ugbeye, Wakio,folu, Joan, Adewole, Ononuro, Kofo,Favour, Mazino, Omaha,Roye, flora, Gboyega, Adedayo } from '../../../public/speakers/img'

const Schedule = () => {
  return (
    <div>
      <section className='container   d-flex justify-content-center align-items-center'>
        <h1 className='col-12 col-md-7 mobile_title text-center my-5 p-2 p-md-0'>TVOC2025 Schedule <br></br> Coming Soon</h1>
      </section>

      {/* <section>
        <div className='container px-md-0 '>
          <div className='d-md-flex align-items-center w-100 p-4 mb-3 shadow-sm rounded border'>
            <span className='cu-orange text-nowrap fw-bold fs-4  me-4'>DAY 1</span>
            <div className='m-0 fs-5 fw-normal'>June 25TH 2024 (WAT)</div>
          </div>
          <div className='gap-3 mb-5 d-flex flex-column'>
            <div className='row row-cols-lg-2 '>
              <Host Img={Femi} speech='Femi Bakes' display='d-none' detail='Event Host'/>
              <Host Img={Kofo} speech='Lady Kofo' display='d-none' detail='Event Host'/>
              <Host Img={Mic} speech='Welcome' detail='Arrival/Commercial Interludes' display='d-flex' duration='12:00pm - 12:30pm'/>
              <Host Img={Mic} speech='MC' detail='MCs INTRO' display='d-flex' duration='12:30pm - 12:35pm'/>
            </div>

            <Commercial title='TVOC how far we have come (sponsors Videos)' duration='12:35 AM - 12:45 PM'/>

            <div className='row row-cols-lg-2 '>
              <Host Img={king} speech='King E Afemikhe (convener)' detail='Welcome speech - the idea behind TVOC2024: DARE' display='d-flex' duration='12:45pm - 12:50pm'/>
              <Host Img={Charles} speech='ACE WONDAS' detail='Poetry Performance' display='d-flex' duration='12:50pm - 12:55pm'/>
            </div>

            <Commercial title='Commercial Break (VOFORCE intro Video)' duration='12:55 PM - 1:00 PM'/>

            <div className='cu-pink w-100'>
              <div className='p-3 d-md-flex justify-content-between align-items-center '>
                <div className='d-flex flex-column flex-md-row justify-content-center align-items-center'>
                  <Image placeholder="blur" src={Joshua} alt='host'/>
                  <div className='ps-2 clash d-flex flex-column justify-content-center  '>
                    <div className='fs-5 cu-orange'>Panel session 1 - Joshua Nana Kwame (Host)</div>
                    <div className='raleway col-md-7'>Topic: Impacting the african economy through voiceover: Building societal growth as voiceover artists</div>
                  </div>
                </div>
                <div className='raleway fs-6 fw-bold cu-orange text-nowrap'>1:00 PM - 2:00 PM</div>
              </div>
            </div>

            <div className='row row-cols-lg-2 '>
              <Panelist 
                Imgesrc={Nadeem} 
                name='Nadeem Khalid' 
                role='Panelist'
                preview='Nadeem, a voice-over artist since 2010, brings
                  tailored creativity to Arabic media daily. His 
                  mentoring efforts with Gravy For The Brain 
                  Arabia elevate emerging talents and invest in
                  industry growth.'
                location='Egypt'
                sign={<CiLocationOn />}
              />
              
              <Panelist 
                Imgesrc={Charlene} 
                name='Charlene Mangweni' 
                role='Panelist'
                preview='Charlene Mangwueni-Furusa is a renowned Zimbabwean screen actor, 
                voiceover artist, and broadcaster with numerous accolades.'
                location='Zimbabwe'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Kamiza} 
                name='Kamiza Chikula' 
                role='Panelist'
                preview='Kamiza Chikula is a consummate communicator with nearly three decades of experience in  every aspect of Marketing, 
                Public Relations and Corporate Communications.'
                location='Zambia'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Kwesi} 
                name='Kwesi Busia' 
                role='Panelist'
                preview='Kwesi B. Busia is a British-Ghanaian law graduate and professional voice actor. 
                Raised in the UK, he began his voice acting career after graduating Summa Cum Laude.'
                location='Ghana'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Wakio} 
                name='Wakio Mzenge' 
                role='Panelist'
                preview='Wakio Mzenge is an award-winning voice, stage and screen actor with vast experience, renowned for her professionalism, 
                craftsmanship and her notable performances in numerous films and TV shows, and on stage.'
                location='Kenya'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Shobo} 
                name='Shobo Oluwaseun' 
                role='Panelist'
                preview='Shobo is an incredibly gifted individual. Fondly called the BrandMASTER, 
                Shobo is a sought-after brand consultant, premium voice over talent, author and an inspirational speaker.'
                location='Nigeria/USA'
                sign={<CiLocationOn />}
              />
            </div>

            <div className='row row-cols-lg-2 '>
              <Host Img={Ayomide} speech='Ayomide Ibrahim' detail=' Achieving financial success as a growing African - The MONEY MINDSET' display='d-flex' duration='2:00pm - 2:30pm'/>
            </div>

            <Commercial title='Commercial Break' duration='2:30 PM - 2:35 PM'/>
            
            <div className='row row-cols-lg-2 '>
              <Host Img={Paula} speech='Paula Gammon Wilson' detail='Auditioning for Success: How to Approach Animation Auditions ' display='d-flex' duration='2:35 PM - 3:35 PM'/>
            </div>

            <div className='cu-pink w-100'>
              <div className='p-3 d-md-flex justify-content-between align-items-center '>
                <div className='d-flex flex-column flex-md-row justify-content-center align-items-center'>
                  <Image placeholder="blur" src={leslie} alt='host'/>
                  <div className='ps-2 clash d-flex flex-column justify-content-center  '>
                    <p className='fs-5 cu-orange'>Panel session 2 - Leslie Olabisi (Host)</p>
                    <p className='raleway col-md-7'>TOPIC: THE BUSINESS OF VOICEOVER: Making money as a voiceover artist </p>
                  </div>
                </div>
                <div className='raleway fs-6 fw-bold cu-orange text-nowrap'>3:35 PM - 4:35 PM</div>
              </div>
            </div>

            <div className='row row-cols-lg-2 '>
              <Panelist 
                Imgesrc={Lau} 
                name='Lau Lapides' 
                role='panelist'
                preview='CEO,  Lau Lapides Company, Casting director, Voice Actor, Coach'
                location='USA'
                sign={<CiLocationOn />}
              />
              <Panelist 
                Imgesrc={Chilu} 
                name='Chilu Lemba' 
                role='panelist'
                preview='With almost 30 years of experience as a voiceover artist, Chilu has established himself as a highly sought-after professional in the industry'
                location='South Africa'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Wakio} 
                name='Wakio Mzenge' 
                role='panelist'
                preview='Wakio Mzenge is an award-winning voice, stage and screen actor with vast experience, renowned for her professionalism, 
                craftsmanship and her notable performances in numerous films and TV shows, and on stage.'
                location='Kenya'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Tim} 
                name='Tim Friedlander' 
                role='panelist'
                preview='Tim Friedlander is a distinguished voice actor and studio owner based in Los Angeles, 
                renowned for his talent and advocacy within the industry.'
                location='USA'
                sign={<CiLocationOn />}
              />
            </div>

            <div className='row row-cols-lg-2 '>
              <Host Img={king} speech='King E x Roye Okupe' detail='Voicing Iyanu, The Animated Series' display='d-flex' duration='4:35 PM - 4:55 PM'/>

              <Panelist 
                Imgesrc={Roye} 
                role='Roye Okupe' 
                preview='Award-winning filmmaker, Screenwriter, Founder, YOUNEEK Studios'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
            </div>

            <div className='row row-cols-lg-2 '>
              <Host Img={Marc} speech='MARC CASHMAN' detail='Practical steps to understanding Narration as a voiceover artist -  Workshop ' display='d-flex' duration='4:55 PM - 5:35 PM'/>
            </div>

            <div className='row row-cols-lg-2 '>
              <Host Img={serah} speech='Serah Johnson x Joan Baker' detail='FIRESIDE CHAT - Unveiling the voiceover acting craft: A Global and Local Perspective' display='d-flex' duration='5:35 PM - 6:15 PM'/>
              <Panelist 
                Imgesrc={Joan} 
                role='Joan Baker' 
                preview=' COO, SOVAS, Author, Actor, Voiceover Artist'
                location='USA'
                sign={<CiLocationOn />}
              />
            </div>
            <Commercial title='MC Closing' duration='6:15 PM - 6:20 PM'/>
          </div>
        </div>
      </section>

      <section>
        <div className='container px-md-0 '>
          <div className='d-md-flex align-items-center w-100 p-4 mb-3 shadow-sm rounded border'>
            <span className='cu-orange text-nowrap fw-bold fs-4  me-4'>DAY 2</span>
            <div className='m-0 fs-5 fw-normal'>June 26TH 2024 (WAT)</div>
          </div>
          <div className='gap-3 mb-5 d-flex flex-column'>
            <div className='row row-cols-lg-2 '>
              <Host Img={oluwaseun} speech='Oluwaseun R. Olusegun' display='d-none' detail='Event Host'/>
              <Host Img={ajayi} speech='Seun Ajayi' display='d-none' detail='Event Host'/>
              <Host Img={Mic} speech='Welcome' detail='Arrival and Registration' display='d-flex' duration=' 9:00 AM - 10:00 AM'/>
              <Host Img={OjSkillz} speech='Ini the Vessel  & OJ Skillz' detail='Nigerian National Anthem' display='d-flex' duration='10:00 AM - 10:10 AM'/>
              <Host Img={segunX} speech='Oluwaseun R. Olusegun & Seun Ajayi' detail='MCs Introduction and Engagement' display='d-flex' duration='10:10 AM - 10:20 AM'/>
            </div>

            <div className='row row-cols-lg-2 '>
              <div className='border border-1 shadow d-md-flex px-0 rounded'>
                <div className='d-flex flex-column flex-md-row align-items-center'>
                    <Image placeholder="blur" src={Gboyega} alt='speaker' width={230} height={230} className=' rounded-md-start '/>
                </div>
                <div className=' d-flex flex-column w-100 h-auto p-3 px-md-5 py-md-4 cu-pink rounded-end justify-content-between h-100'>
                  <div className='text-black'>
                    <h6 className='clash_light  '> 
                        <span className='ft me-3 fw-light p-2 lightOrage'>Gboyega Akosile</span>
                        <div className='ft me-3 fw-light pt-3'>Special Adviser to the governor of Lagos state on Media and Publicity</div>
                    </h6>
                    <p className=' mb-3 fw-bold ft-3'>KEYNOTE SPEECH</p>
                  </div>
                  <div className='raleway cu-orange'>
                    10:20 AM - 10:25 AM
                  </div>
                </div>
              </div>

              <Host Img={Omaha} speech='Omaha The StoryTeller' detail='Poetry Performance' display='d-flex' duration='10:25 AM - 10:30 AM'/>
            </div>

            <Commercial title='Sponsor Showcase (JAMMIT STUDIOS)' duration='10:30 AM - 10:35 AM'/>
            <Commercial title='Games/networking moment (MCS)' duration='10:35 AM - 11:00 AM'/>

            <div className='row row-cols-lg-2 '>
              <Host Img={flora} speech='Flora Gabtony' detail='Professional ethics efficiency for a successful career' display='d-flex' duration='11:00 AM - 11:40 NOON'/>
              <Host Img={flora} speech='Flora Gabtony' detail='Q & A with Flora Gabtony' display='d-flex' duration='11:40 NOON - 11:50 NOON'/>
              <Host Img={wale} speech='Wale Ekundayo' detail='Negotiation and how to keep clients returning' display='d-flex' duration='11:50 PM - 12:30 PM'/>
            </div>

            <Commercial title='Sponsor Presentation - Waystream ' duration='12:30 PM - 12:35 PM'/>

            <div className='cu-pink w-100'>
              <div className='p-3 d-md-flex justify-content-between align-items-center '>
                <div className='d-flex flex-column flex-md-row justify-content-center align-items-center'>
                  <Image placeholder="blur" src={Favour} alt='host' width={120} height={120} className='bg-black'/>
                  <div className='ps-2 clash d-flex flex-column justify-content-center  '>
                    <p className='fs-5 cu-orange'>Panel session 1- Favour Ime (Host)</p>
                    <p className='raleway col-md-7'>TELLING AFRICAN STORIES THROUGH TECHNOLOGY, VOICEOVER AND ANIMATION</p>
                  </div>
                </div>
                <div className='raleway fs-6 fw-bold cu-orange text-nowrap'>12:35 PM - 1:25 PM</div>
              </div>
            </div>

            <div className='row row-cols-lg-2 '>
              <Panelist 
                Imgesrc={Ferdinand} 
                name='Ferdinand Amefiele' 
                role='Panelist'
                preview=''
                location='Kenya'
                sign={<CiLocationOn />}
              />
              <Panelist 
                Imgesrc={Ononuro} 
                name='Oghenekevwe Ononuro' 
                role='Panelist'
                preview='CEO, Kynetics, 2D Animator, Storyteller'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
              <Panelist 
                Imgesrc={Esther} 
                name='Esther Kemi  Gbadamosi' 
                role='Panelist'
                preview='Esther Kemi Gbadamosi is the Founder/Creative Director of Radioxity Media. 
                A writer, cinematographer, editor and stop motion animator.'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
              <Panelist 
                Imgesrc={koromone} 
                name='Koromone Asabe-Yobaere' 
                role='Panelist'
                preview='KAY is a poet, audio storyteller, and multimedia journalist who enjoys telling stories about Black love, 
                redemption, personal growth, and delicious romance. '
                location='Nigeria'
                sign={<CiLocationOn />}
              />
            </div>

            <div className='row row-cols-lg-2'>
              <Host Img={Adewole} speech='Mr. Wols & the VO Artist' detail='The Power of Sound Design in Voiceover ' display='d-flex' duration='1:25 PM - 2:10 PM'/>
            </div>

            <div className='cu-pink w-100'>
              <div className='p-3 d-md-flex justify-content-between align-items-center '>
                <div className='d-flex flex-column flex-md-row justify-content-center align-items-center'>
                  <Image placeholder="blur" src={Mazino} alt='host' width={120} height={120} className='bg-black'/>
                  <div className='ps-2 clash d-flex flex-column justify-content-center  '>
                    <p className='fs-5 cu-orange'>Panel session 2- Mazino Appeal (Host)</p>
                    <p className='raleway col-md-7'>THE CLIENT, YOUR CRAFT AND YOU: Bringing the gap between your voice, client and business</p>
                  </div>
                </div>
                <div className='raleway fs-6 fw-bold cu-orange text-nowrap'>2:10 PM - 3:00 PM</div>
              </div>
            </div>
            
            <div className='row row-cols-lg-2 '>
              <Panelist 
                Imgesrc={wale} 
                name='Wale Ekundayo                ' 
                role='Panelist'
                preview='CEO/Vice President, Cerebre Media Africa, Founder, LealFC'
                location='Nigeria'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={folu} 
                name='Folu Storms' 
                role='Panelist'
                preview='Actor, Voiceover Artist, T.V./Event Host'
                location='Nigeria'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Ugbeye} 
                name='Stephanie Ugbeye' 
                role='Panelist'
                preview='Creative Writer, Producer, Singer, Screen & Voice Actor, Content Creator'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
            </div>
            
            <Commercial title='Sponsor Presentation - Called out Estates Limited' duration='3:00 PM - 3:10 PM'/>

            <div className='row row-cols-lg-2'> 
              <div className='border border-1 shadow d-md-flex px-0 rounded'>
                <div className='d-flex flex-column flex-md-row align-items-center'>
                    <Image placeholder="blur" src={Adedayo} alt='speaker' width={230} height={230} className=' rounded-md-start '/>
                </div>
                <div className=' d-flex flex-column w-100 h-auto p-3 px-md-5 py-md-4 cu-pink rounded-end justify-content-between h-100'>
                  <div className='text-black'>
                    <h6 className='clash_light  '> 
                        <span className='ft me-3 fw-light p-2 lightOrage'>Adedayo Adejokun (Host)</span>
                    </h6>
                    <p className='cu-orange fw-bold mt-3'>FIRESIDE CHAT with King E Afemikhe</p>
                    <p className=' mb-3 ft-3'>Topic: Breaking through the hurdles: achieving VO success in a competitive age</p>
                  </div>
                  <div className='raleway cu-orange'>
                    3:10 PM - 3:40 PM
                  </div>
                </div>
              </div>

              <Panelist 
                Imgesrc={king} 
                role='King E Afemikhe (convener)' 
                preview='Founder, Voiceover Workshop and Media; Convener, The Voiceover Conference. With over 13 years of experience as a voiceover artist, producer, and trainer, 
                I strive to make a mark in the African voiceover industry.'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
            </div>

            <Commercial title='CJ Kings - StoryTeller - Presentation' duration='3:40 PM - 3:50 PM'/>

            <div className='row row-cols-lg-2'> 
              <div className='cu-pink w-100'>
                <div className='p-3 d-md-flex justify-content-between align-items-center '>
                  <div className='d-flex flex-column flex-md-row justify-content-center align-items-center'>
                    <Image placeholder="blur" src={tolulope} alt='host'/>
                    <div className='ps-2 clash d-flex flex-column justify-content-center  '>
                      <p className='fs-5 cu-orange'>Panel session 3- Tolulope Kolade (Host)</p>
                      <p className='raleway col-md-7'>THE CREATOR ECONOMY: Social media, content creation and the future of voiceover in Africa </p>
                    </div>
                  </div>
                  <div className='raleway fs-6 fw-bold cu-orange text-nowrap'>3:50 PM - 4:50 PM</div>
                </div>
              </div>
            </div>

            <div className='row row-cols-lg-2 '>
              <Panelist 
                Imgesrc={Nicholas} 
                name='Stephanie Nicholas' 
                role='Panelist'
                preview='Award Winning Voiceover Artist, Event Host, Content Creator'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
              <Panelist 
                Imgesrc={David} 
                name='David Attah ' 
                role='Panelist'
                preview='Voiceover Artist, Content Creator, Coach'
                location='Nigeria'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Charles} 
                name='Charles Alade' 
                role='Panelist'
                preview='Founder, Voice Over Bootcamp, Media Personality, Voiceover Coach'
                location='Nigeria'
                sign={<CiLocationOn />}
              />

              <Panelist 
                Imgesrc={Eniola} 
                name='Eniola Keshinro' 
                role='Panelist'
                preview='Voice Actor, Coach, Event Host'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
              <Panelist 
                Imgesrc={Edewor} 
                name='Edewor Ajueshi ' 
                role='Panelist'
                preview='Tv presenter and producer'
                location='Nigeria'
                sign={<CiLocationOn />}
              />
            </div>

            <Commercial title='TVOC NEW LOOK REVEAL - LEXAIN' duration='4:50 PM - 5:00 PM'/>
            <Commercial title='CAKE CUTTING - TVOC @5TH  YEAR' duration=' 5:00 PM - 5:20 PM'/>

            <div className='cu-pink w-100'>
              <div className='p-3 d-md-flex justify-content-between align-items-center '>
                <div className='d-flex flex-column flex-md-row justify-content-center align-items-center'>
                  <Image placeholder="blur" src={king2} alt='host'/>
                  <div className='ps-2 clash d-flex flex-column justify-content-center  '>
                    <p className='fs-5 cu-orange'>Closing Remark- King E Afemikhe</p>
                    <p className='raleway'>TVOC2025 date announcement</p>
                  </div>
                </div>
                <div className='raleway fs-6 fw-bold cu-orange text-nowrap ps-2'>5:20 PM - 6:00 PM</div>
              </div>
            </div>
          </div>
        </div>
      </section> */}
    </div>
  )
}

export default Schedule
