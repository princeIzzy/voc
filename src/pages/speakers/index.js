'use client'
import { useEffect } from 'react';
import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';
import {remark} from 'remark';
import html from 'remark-html';
import Image from 'next/image';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { CiLocationOn } from 'react-icons/ci';

export default function Speakers({ speakers }) {
  useEffect(() => {
    AOS.init({
      duration: 1500,
      once: true,
    });
  }, []);

  // Sort speakers based on the 'order' property
  const sortedSpeakers = speakers.sort((a, b) => a.order - b.order);
  return (
    <div>
      <section className='  my-auto'>
        <div className='speaker-hero d-flex align-items-center justify-content-center'>
          <h1 className='mobile_title text-white z-1 w-75 w-md-50 fw-bold text-center ' data-aos="fade-in">
            {/* Speakers */}
            Coming soon!
          </h1>
        </div>
      </section>

      {/* <section className='content_space2'>
        <div className='container px-md-0'>
          <div className='grid-container align-items-center'>
            {sortedSpeakers.map((speaker, index) => (
              <div
                key={index}
                className='border border-1 shadow-sm d-lg-flex px-0 rounded overflow-hidden'
                data-bs-toggle="modal"
                data-bs-target={`#exampleModal-${index}`}
              >
                <div className='d-flex justify-content-center pt-5 pt-md-0' data-aos="fade-zoom-in">
                  <Image src={speaker.image} alt={speaker.name} width={230} height={220} className='rounded-start' />
                </div>
                <div className='cover d-flex flex-column h-auto p-3 px-md-5 py-md-4 justify-content-between'>
                  <div className='pt-2 mb-3 mb-md-0'>
                    <h5 className='clash_light fw-bolder' data-aos="fade-zoom-in" data-aos-easing="linear">{speaker.name}</h5>
                    <p className='mb-0 ft-1'>{speaker.preview}</p>
                  </div>
                  <div className='me-auto px-2 py-1 ft-1 lightOrage'>
                    <span className='me-1'><CiLocationOn /></span>{speaker.location}
                  </div>
                </div>
              </div>
            ))}

            {sortedSpeakers.map((speaker, index) => (
              <div
                key={index}
                className="modal fade"
                id={`exampleModal-${index}`}
                tabIndex="-1"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
              >
                <div className="modal-dialog modal-dialog-centered">
                  <div className="modal-content">
                    <div className="modal-body p-4">
                      <div className="">
                        <div className="float-lg-start d-flex justify-content-center me-3 mb-2">
                          <Image src={speaker.image} alt={speaker.name} width={230} height={220} className='rounded-start' />
                        </div>
                        <div className="d-flex pt-4">
                          <h5 className="modal-title" id="exampleModalLabel">{speaker.name}</h5>
                          <div className='ms-3 px-2 py-1 ft-1 lightOrage'>
                            <span className='me-1'><CiLocationOn /></span>{speaker.location}
                          </div>
                        </div>
                        <div className="flex-grow-1">
                          <div className='pt-2 mb-3'>
                            <p className='mb-0 ft-1'>{speaker.preview}</p>
                          </div>
                          <div className='modal-text' dangerouslySetInnerHTML={{ __html: speaker.contentHtml }} />
                        </div>
                      </div>
                    </div>
                    <div className="modal-footer d-lg-none">
                      <button type="button" className="btn register_btn orange text-white" data-bs-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section> */}
    </div>
  );
}

export async function getStaticProps() {
  const speakersDirectory = path.join('public', 'speakers');
  const filenames = fs.readdirSync(speakersDirectory);

  const speakers = await Promise.all(
    filenames.map(async (filename) => {
      const filePath = path.join(speakersDirectory, filename);

      // Only process Markdown files
      if (filename.endsWith('.md')) {
        const fileContents = fs.readFileSync(filePath, 'utf8');
        const { data, content } = matter(fileContents);
        const processedContent = await remark().use(html).process(content);
        const contentHtml = processedContent.toString();

        // Ensure that image reference is correct
        const image = data.image;

        return { ...data, contentHtml, image };
      } else {
        return null;
      }
    })
  );

  // Filter out null values (non-Markdown files)
  const filteredSpeakers = speakers.filter(speaker => speaker !== null);

  return {
    props: {
      speakers: filteredSpeakers,
    },
  };
}
